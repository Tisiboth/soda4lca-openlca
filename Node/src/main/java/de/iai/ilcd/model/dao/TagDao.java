package de.iai.ilcd.model.dao;

import java.util.List;

import javax.persistence.EntityManager;

import de.iai.ilcd.model.tag.Tag;
import de.iai.ilcd.persistence.PersistenceUtil;

public class TagDao extends AbstractLongIdObjectDao<Tag> {

	public TagDao() {
		super(Tag.class.getSimpleName(), Tag.class);
	}
	
	public List<Tag> getTagsAsc() {
		EntityManager em = PersistenceUtil.getEntityManager();
		return em.createQuery("SELECT a FROM " + this.getJpaName() + " a ORDER BY a.name ASC").getResultList();
	}
	
	public Tag getTagByName(String name) {
	    for (Tag t : this.getAll()) {
	        if (t.getName().equals(name)) {
	            return t;
	        }
	    }
	    return null;
	}
	
}
