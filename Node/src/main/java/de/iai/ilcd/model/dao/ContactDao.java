package de.iai.ilcd.model.dao;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.velocity.tools.generic.ValueParser;

import de.fzk.iai.ilcd.api.app.contact.ContactDataSet;
import de.fzk.iai.ilcd.service.client.impl.DatasetTypes;
import de.fzk.iai.ilcd.service.model.IContactListVO;
import de.fzk.iai.ilcd.service.model.IContactVO;
import de.iai.ilcd.model.common.DataSet;
import de.iai.ilcd.model.common.DataSetType;
import de.iai.ilcd.model.contact.Contact;
import de.iai.ilcd.model.datastock.RootDataStock;
import de.iai.ilcd.util.UnmarshalHelper;

/**
 * Data access object for {@link Contact contacts}
 */
public class ContactDao extends DataSetDao<Contact, IContactListVO, IContactVO> {

	/**
	 * Create the data access object for {@link Contact contacts}
	 */
	public ContactDao() {
		super( "Contact", Contact.class, IContactListVO.class, IContactVO.class, DataSetType.CONTACT );
	}

	@Override
	protected String getDataStockField() {
		return DatasetTypes.CONTACTS.getValue();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void preCheckAndPersist( Contact dataSet ) {
		// Nothing to do for contacts :)
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String getQueryStringOrderJpql( String typeAlias, String sortString, boolean sortOrder ) {
		if ( "email".equals( sortString ) ) {
			return buildOrderBy(typeAlias, "email", typeAlias, "nameCache", sortOrder);
		}
		else if ( "www".equals( sortString ) ) {
			return buildOrderBy(typeAlias, "www", typeAlias, "nameCache", sortOrder);
		}
		else {
			return super.getQueryStringOrderJpql( typeAlias, sortString, sortOrder );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void addWhereClausesAndNamedParamesForQueryStringJpql( String typeAlias, ValueParser params, List<String> whereClauses,
			Map<String, Object> whereParamValues ) {
		// nothing to do beside the defaults
	}

	/* (non-Javadoc)
	 * @see de.iai.ilcd.model.dao.DataSetDao#getDependencies(de.iai.ilcd.model.common.DataSet, de.iai.ilcd.model.dao.DependenciesMode)
	 */
	@Override
	public Set<DataSet> getDependencies(DataSet dataset, DependenciesMode mode) {

		Set<DataSet> dependencies = new HashSet<DataSet>();
		Contact contact = (Contact) dataset;
		RootDataStock stock = contact.getRootDataStock();

		ContactDataSet xmlDataset = (ContactDataSet) new UnmarshalHelper().unmarshal(contact);
		
		try {
			addDependencies(xmlDataset.getAdministrativeInformation().getDataEntryBy().getReferenceToDataSetFormat(), stock, dependencies);
		} catch (Exception e) {
		}
		try {
			addDependency(xmlDataset.getAdministrativeInformation().getPublicationAndOwnership()
					.getReferenceToOwnershipOfDataSet(), stock, dependencies);
		} catch (Exception e) {
		}
		try {
			addDependencies(xmlDataset.getContactInformation().getDataSetInformation().getReferenceToContact(), stock, dependencies);
		} catch (Exception e) {
		}
		try {
			addDependency(xmlDataset.getContactInformation().getDataSetInformation().getReferenceToLogo(), stock, dependencies);
		} catch (Exception e) {
		}
		return dependencies;
	}
}
