package de.iai.ilcd.model.process;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import de.fzk.iai.ilcd.service.model.common.IMultiLangString;
import de.fzk.iai.ilcd.service.model.enums.LicenseTypeValue;
import de.fzk.iai.ilcd.service.model.process.IAccessInformation;
import de.iai.ilcd.util.lstring.IStringMapProvider;
import de.iai.ilcd.util.lstring.MultiLangStringMapAdapter;

/**
 * 
 * @author clemens.duepmeier
 */
@Entity
@Table( name = "process_accessinformation" )
public class AccessInformation implements Serializable, IAccessInformation {

	/**
	 * Serialization ID
	 */
	private static final long serialVersionUID = 6587910669693621030L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	private Long id;

	/**
	 * Get the ID of Access information
	 * 
	 * @return the ID of Access information
	 */
	public Long getId() {
		return this.id;
	}

	/**
	 * Set the ID of Access information
	 * 
	 * @param id
	 *            the ID of Access information to set
	 */
	public void setId( Long id ) {
		this.id = id;
	}
	protected boolean copyright = false;

	@Enumerated( EnumType.STRING )
	protected LicenseTypeValue licenseType;

	@ElementCollection
	@Column( name = "value", columnDefinition = "TEXT" )
	@CollectionTable( name = "process_userestrictions", joinColumns = @JoinColumn( name = "process_accessinformation_id" ) )
	@MapKeyColumn( name = "lang" )
	protected final Map<String, String> useRestrictions = new HashMap<String, String>();

	/**
	 * Adapter for API backwards compatibility.
	 */
	@Transient
	private final MultiLangStringMapAdapter useRestrictionsAdapter = new MultiLangStringMapAdapter( new IStringMapProvider() {

		@Override
		public Map<String, String> getMap() {
			return AccessInformation.this.useRestrictions;
		}
	} );

	@Override
	public boolean isCopyright() {
		return this.copyright;
	}

	public void setCopyright( boolean copyright ) {
		this.copyright = copyright;
	}

	@Override
	public LicenseTypeValue getLicenseType() {
		return this.licenseType;
	}

	public void setLicenseType( LicenseTypeValue licenseType ) {
		this.licenseType = licenseType;
	}

	@Override
	public IMultiLangString getUseRestrictions() {
		return this.useRestrictionsAdapter;
	}

	public void setUseRestrictions( IMultiLangString useRestrictions ) {
		this.useRestrictionsAdapter.overrideValues( useRestrictions );
	}
}
