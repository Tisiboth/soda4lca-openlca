package de.iai.ilcd.model.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import de.iai.ilcd.model.common.JobMetaData;
import de.iai.ilcd.persistence.PersistenceUtil;

/**
 * The Data Access Object (DAO) for {@link JobMetaData}
 * 
 * @author sarai
 *
 */
public class JobMetaDataDao extends AbstractLongIdObjectDao<JobMetaData> {

	/**
	 * The dao to Push Target entry
	 */
	public JobMetaDataDao() {
		super("JobMetaData", JobMetaData.class);
	}

	/**
	 * Gets list of Push Target entries.
	 * 
	 * @return List of push target entries
	 */
	public List<JobMetaData> getJobMetaDataList() {
		return this.getAll();
	}

	/**
	 * Gets push target entry with given name.
	 * 
	 * @param name
	 *            The name of desired push target entry
	 * @return a Push Target entry with given name
	 */
	public JobMetaData getJobMetaData(String jobId) {
		EntityManager em = PersistenceUtil.getEntityManager();
		JobMetaData jobMetaData = null;
		try {
			jobMetaData = (JobMetaData) em.createQuery("select j from JobMetaData j where j.jobId=:jobId")
					.setParameter("jobId", jobId).getSingleResult();
		} catch (NoResultException nre) {
		}
		return jobMetaData;
	}

	/**
	 * Gets Push Target entry with given id.
	 * 
	 * @param id
	 *            The id of desired push target
	 * @return A push target entry with given id
	 */
	public JobMetaData getJobMetaData(Long id) {
		return this.getById(id);
	}

}
