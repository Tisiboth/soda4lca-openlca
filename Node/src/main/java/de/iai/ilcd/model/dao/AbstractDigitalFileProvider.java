package de.iai.ilcd.model.dao;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.iai.ilcd.util.CaseInsensitiveFilenameFilter;
import de.iai.ilcd.util.SodaUtil;
import net.java.truevfs.access.TFile;
import net.java.truevfs.access.TFileInputStream;

/**
 * Provider for the digital files of an Source data set
 */
public abstract class AbstractDigitalFileProvider {

	/**
	 * Logger
	 */
	private static final Logger logger = LoggerFactory.getLogger( AbstractDigitalFileProvider.class );

	/**
	 * Get the plain filename without directories
	 * 
	 * @param digitalFileReference
	 *            reference to digital file
	 * @return plain filename without directories
	 */
	public String getBasename( String digitalFileReference ) {
		return FilenameUtils.getName( digitalFileReference );
	}

	/**
	 * Determine if digital file can be provided
	 * 
	 * @param digitalFileReference
	 *            reference to digital file
	 * @return <code>true</code> if digital file can be provided, <code>false</code> otherwise
	 */
	public abstract boolean hasDigitalFile( String digitalFileReference );

	/**
	 * Get input stream for given name
	 * 
	 * @param digitalFileReference
	 *            reference to digital file
	 * @return input stream for the name or <code>null</code> if none present
	 * @throws IOException
	 *             on I/O errors
	 */
	public abstract InputStream getInputStream( String digitalFileReference ) throws IOException;

	/**
	 * Provider for digital files from file system
	 */
	public static class FileSystemDigitalFileProvider extends AbstractDigitalFileProvider {

		/**
		 * Base directory to search for files
		 */
		private final String baseDirectory;

		/**
		 * Create the provider
		 * 
		 * @param datasetFile
		 *            the data set file to create for
		 */
		public FileSystemDigitalFileProvider( String datasetFile ) {
			logger.debug( "raw source data set file name is {}", datasetFile );
			this.baseDirectory = FilenameUtils.getFullPath( datasetFile );
		}

		/**
		 * Get the full file path
		 * 
		 * @param digitalFileReference
		 *            reference
		 * @return full file path
		 */
		private String getFullFilePath( String digitalFileReference ) {
			// get rid of any leading/trailing white space
			digitalFileReference = StringUtils.trimToNull( digitalFileReference );
			if ( digitalFileReference != null ) {
				String absoluteFileName = FilenameUtils.concat( this.baseDirectory, digitalFileReference );
				logger.debug( "normalized form of digital file name is {}", absoluteFileName );
				logger.debug( "reference to digital file: " + absoluteFileName );
				return absoluteFileName;
			}
			return null;
		}

		/**
		 * Get the file path
		 * 
		 * @param digitalFileReference
		 *            reference
		 * @return file path or <code>null</code> if nothing found
		 */
		private String getFilePath( String digitalFileReference ) {
			
			// nothing to do if it's a URL
			if (digitalFileReference!=null && (digitalFileReference.trim().toLowerCase().startsWith("https://") || digitalFileReference.trim().toLowerCase().startsWith("http://")))
					return null;
			
			TFile file = new TFile( this.getFullFilePath( digitalFileReference ) );
			if ( file.canRead() ) {
				return file.getAbsolutePath();
			}
			else if ( SodaUtil.hasValidExtension( digitalFileReference ) ) {
				// in case we're on a case sensitive file system and the
				// case of the name / extension is not correct or "../external_docs/"
				// prefix is missing, we'll try to fix this

				String fileNotFound = file.getName();
				String[] caseMatches = file.getParentFile().list( new CaseInsensitiveFilenameFilter( fileNotFound ) );
				String[] prefixMatches = new TFile( file.getParentFile().toString() + "/../external_docs/" ).list( new CaseInsensitiveFilenameFilter(
						fileNotFound ) );

				if ( ArrayUtils.getLength( caseMatches ) == 1 ) {
					return FilenameUtils.concat( file.getParentFile().getAbsolutePath(), caseMatches[0] );
				}
				else if ( ArrayUtils.getLength( prefixMatches ) == 1 ) {
					logger.warn( "file " + fileNotFound + " could not be located at indicated path, but was found in external_docs folder" );
					return FilenameUtils.concat( baseDirectory + "../external_docs", prefixMatches[0] );
				}
				else {
					return null;
				}
			}
			logger.error( "file could not be read from " + file.getAbsolutePath() );

			// Not a valid local filename
			return null;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean hasDigitalFile( String digitalFileReference ) {
			return this.getFilePath( digitalFileReference ) != null;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public InputStream getInputStream( String digitalFileReference ) throws IOException {
			final String path = this.getFilePath( digitalFileReference );
			if ( path != null ) {
				return new TFileInputStream( path );
			}
			return null;
		}

	}
}