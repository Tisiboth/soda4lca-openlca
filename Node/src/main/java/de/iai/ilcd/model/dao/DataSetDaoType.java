package de.iai.ilcd.model.dao;

import java.util.Collection;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.Set;

import de.iai.ilcd.model.common.DataSet;
import de.iai.ilcd.model.common.DataSetType;
import de.iai.ilcd.model.flow.ElementaryFlow;
import de.iai.ilcd.model.flow.ProductFlow;

public enum DataSetDaoType {
	
	CONTACT( new ContactDao(), DataSetType.CONTACT),
	ELEMENTARY_FLOW( new ElementaryFlowDao(), DataSetType.FLOW),
	PRODUCT_FLOW( new ProductFlowDao(), DataSetType.FLOW),
	FLOW_PROPERTY( new FlowPropertyDao(), DataSetType.FLOWPROPERTY),
	LCIA_METHOD( new LCIAMethodDao(), DataSetType.LCIAMETHOD),
	LIFE_CYCLE_MODEL( new LifeCycleModelDao(), DataSetType.LIFECYCLEMODEL),
	PROCESS( new ProcessDao(), DataSetType.PROCESS),
	SOURCE( new SourceDao(), DataSetType.SOURCE),
	UNIT_GROUP( new UnitGroupDao(), DataSetType.UNITGROUP);
	
	DataSetDao<?,?,?> dao;
	DataSetType dsType;
	
	DataSetDaoType(DataSetDao<?,?,?> dao, DataSetType dsType) {
		this.dao = dao;
		this.dsType = dsType;
	}
	
	public static DataSetDaoType getFor(DataSet ds) {
		if (ds == null || ds.getDataSetType() == null)
			return null;
		
		// DataSetType.Flow is not sufficient (there's two possible daos responsible)..
		if (ds instanceof ElementaryFlow) {
			return ELEMENTARY_FLOW;
		} else if (ds instanceof ProductFlow) {
			return PRODUCT_FLOW;
			
		} else {
			// data set type is sufficient..
			
			for (DataSetDaoType daoType : DataSetDaoType.values())
				if (daoType.getDsType().equals(ds.getDataSetType()))
					return daoType;
		}
		
		// else we have found nothing.
		return null;
	}
	
	public static EnumMap<DataSetDaoType, Set<DataSet>> groupByDaoType(Collection<DataSet> dsCollection) {
		EnumMap<DataSetDaoType, Set<DataSet>> invertedTypeMap = new EnumMap<>(DataSetDaoType.class);
		
		// populate the map with empty sets
		for (DataSetDaoType type : DataSetDaoType.values()) {
			invertedTypeMap.put(type, new HashSet<DataSet>());
		}
		
		// sort the data sets
		for (DataSet ds : dsCollection) {
			DataSetDaoType daoType = DataSetDaoType.getFor(ds);
			Set<DataSet> set = invertedTypeMap.get(daoType);
			set.add(ds);
			invertedTypeMap.put(daoType, set);
		}
		
		// remove entries with no data sets
		for (DataSetDaoType type : invertedTypeMap.keySet()) {
			if (invertedTypeMap.get(type).isEmpty())
				invertedTypeMap.remove(type);
		}

		return invertedTypeMap;
	}
	
	public DataSetDao<?,?,?> getDao() {
		return this.dao;
	}
	
	private DataSetType getDsType() {
		return this.dsType;
	}
	
}
