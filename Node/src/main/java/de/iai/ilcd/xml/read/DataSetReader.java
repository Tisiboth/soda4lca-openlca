package de.iai.ilcd.xml.read;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.xml.JDOMParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.fzk.iai.ilcd.service.model.common.ILString;
import de.fzk.iai.ilcd.service.model.common.IMultiLangString;
import de.iai.ilcd.model.common.Classification;
import de.iai.ilcd.model.common.DataSet;
import de.iai.ilcd.model.common.DataSetType;
import de.iai.ilcd.model.common.DataSetVersion;
import de.iai.ilcd.model.common.Language;
import de.iai.ilcd.model.common.Uuid;
import de.iai.ilcd.model.common.XmlFile;
import de.iai.ilcd.model.common.exception.FormatException;
import de.iai.ilcd.model.dao.LanguageDao;
import net.java.truevfs.access.TFile;
import net.java.truevfs.access.TFileInputStream;

/**
 * Reader for data sets
 * 
 * @param <T>
 *            type of data set
 */
public abstract class DataSetReader<T extends DataSet> {

	/**
	 * Logger
	 */
	private static final Logger logger = LoggerFactory.getLogger( DataSetReader.class );

	/**
	 * Parsing helper
	 */
	protected DataSetParsingHelper parserHelper = null;

	/**
	 * Reader for common constructs
	 */
	protected CommonConstructsReader commonReader = null;

	protected LanguageDao languageDao = new LanguageDao();

	/**
	 * Parse the data set
	 * 
	 * @param context
	 *            context
	 * @param out
	 *            output writer
	 * @return parsed data set
	 */
	protected abstract T parse( JXPathContext context, PrintWriter out );

	/**
	 * Read data set from file
	 * 
	 * @param fileName
	 *            name of the file
	 * @param out
	 *            output writer
	 * @return created data set
	 * @throws FileNotFoundException
	 *             if file was not found
	 * @throws IOException
	 *             on I/O errors
	 * @throws UnsupportedEncodingException
	 *             if encoding is not supported
	 */
	public T readFromFile( String fileName, PrintWriter out ) throws FileNotFoundException, IOException, UnsupportedEncodingException {
		TFile file = new TFile( fileName );
		return this.readDataSetFromFile( file, out );
	}

	/**
	 * Read data set from file
	 * 
	 * @param file
	 *            file to read from
	 * @param out
	 *            output writer
	 * @return created data set
	 * @throws FileNotFoundException
	 *             if file was not found
	 * @throws IOException
	 *             on I/O errors
	 * @throws UnsupportedEncodingException
	 *             if encoding is not supported
	 */
	public T readDataSetFromFile( TFile file, PrintWriter out ) throws FileNotFoundException, IOException, UnsupportedEncodingException {
		if ( file == null ) {
			out.println( "Warning: file must not be null" );
			throw new FileNotFoundException( "Warning: file must not be null" );
		}

		T dataSet = null;

		TFileInputStream inputStream = null;
		try {
			inputStream = new TFileInputStream( file );
		}
		catch ( Exception e ) {
			out.println( "Warning: Cannot find file " + file.getName() );
			throw new FileNotFoundException( "File " + file.getName() + " cannot be found" );
		}
		dataSet = this.readDataSetFromStream( inputStream, out );
		inputStream.close();

		return dataSet;
	}


	/**
	 * Read data set from stream
	 * 
	 * @param inStream
	 *            input stream
	 * @param out
	 *            output writer
	 * @return created data set
	 * @throws IOException
	 *             on I/O errors
	 * @throws UnsupportedEncodingException
	 *             if encoding is not supported
	 */
	public T readDataSetFromStream( InputStream inStream, PrintWriter out ) throws IOException, UnsupportedEncodingException {
		byte[] contents = null;
		try {
			contents = IOUtils.toByteArray( inStream );
		}
		catch ( IOException e ) {
			logger.error( "cannot read the whole input stream into byte array" );
			throw e;
		}
		IOUtils.closeQuietly( inStream );

		InputStream inBytes = new ByteArrayInputStream( contents );
		T dataSet = this.parseStream( inBytes, out );
		IOUtils.closeQuietly( inBytes );

		XmlFile xmlFile = new XmlFile();
		String xml;
		try {
			xml = new String( contents, "UTF-8" );
		}
		catch ( UnsupportedEncodingException e ) {
			logger.error( "cannot parse stream input into an UTF-8 string" );
			logger.error( "Exception is: ", e );
			throw e;
		}
		// if BOM is in front of ByteArray, we may have some unknown characters in front of <xml because
		// UTF-8 conversion in Standard Java has problems with BOM, let's just filter these nasty characters
		if ( !xml.startsWith( "<" ) )
		{
			xml = xml.trim().replaceFirst( "^([\\W]+)<", "<" ); // remove junk at front of dataset
		}
		xmlFile.setContent( xml );
		dataSet.setXmlFile( xmlFile );

		return dataSet;
	}

	/**
	 * Parse input stream
	 * 
	 * @param inputStream
	 *            input stream to parse
	 * @param out
	 *            output writer
	 * @return parsed data set
	 */
	public T parseStream( InputStream inputStream, PrintWriter out ) {
		JDOMParser parser = new JDOMParser();
		parser.setValidating( false );
		Object doc = parser.parseXML( inputStream );
		JXPathContext context = JXPathContext.newContext( doc );
		context.setLenient( true );
		context.registerNamespace( "common", "http://lca.jrc.it/ILCD/Common" );

		this.parserHelper = new DataSetParsingHelper( context );
		this.commonReader = new CommonConstructsReader( this.parserHelper );

		return this.parse( context, out );
	}

	/**
	 * Read the common fields
	 * 
	 * @param dataset
	 *            data set to assign values to
	 * @param dataSetType
	 *            type of the data set
	 * @param context
	 *            context
	 * @param out
	 *            output writer
	 */
	public void readCommonFields( DataSet dataset, DataSetType dataSetType, JXPathContext context, PrintWriter out ) {

		IMultiLangString name = this.parserHelper.getIMultiLanguageString( "/*/*[1]/*[local-name()='dataSetInformation']/common:name" );
		Uuid uuid = this.commonReader.getUuid();
		IMultiLangString generalComment = this.parserHelper.getIMultiLanguageString( "/*/*[1]/*[local-name()='dataSetInformation']/common:generalComment" );
		String permanentUri = this.parserHelper.getStringValue( "/*/*[local-name()='administrativeInformation']/*[local-name()='publicationAndOwnership']/common:permanentDataSetURI" );
		String versionString = this.parserHelper.getStringValue( "/*/*[local-name()='administrativeInformation']/*[local-name()='publicationAndOwnership']/common:dataSetVersion" );

		dataset.setName( name );

		// set supported languages
		for ( ILString l : name.getLStrings() ) {
			Language lang = languageDao.getByLanguageCode( l.getLang() );
			dataset.getSupportedLanguages().add( lang );
		}

		dataset.setUuid( uuid );
		dataset.setDescription( generalComment );
		dataset.setPermanentUri( permanentUri );
		DataSetVersion version = new DataSetVersion();
		try {
			if ( versionString != null ) {
				version = DataSetVersion.parse( versionString );
			}
			else {
				if ( out != null ) {
					out.println( "Warning: This data set has no version number; version will be set to 00.00.000" );
				}
			}
		}
		catch ( FormatException ex ) {
			if ( out != null ) {
				out.println( "Warning: The version number has an invalid format. See below for details." );
				out.println( "Exception output: " + ex.getMessage() );
			}
		}
		dataset.setVersion( version );

		// get the list of classifications and add them
		List<Classification> classifications = this.commonReader.getClassifications( dataSetType );
		for ( Classification c : classifications ) {
			dataset.addClassification( c );
		}
	}

	public DataSetParsingHelper getParserHelper() {
		return parserHelper;
	}

	public void setParserHelper(DataSetParsingHelper parserHelper) {
		this.parserHelper = parserHelper;
	}

	public CommonConstructsReader getCommonReader() {
		return commonReader;
	}

	public void setCommonReader(CommonConstructsReader commonReader) {
		this.commonReader = commonReader;
	}
}
