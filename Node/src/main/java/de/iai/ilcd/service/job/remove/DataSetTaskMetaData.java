package de.iai.ilcd.service.job.remove;

import de.iai.ilcd.model.common.DataSet;
import de.iai.ilcd.service.job.TaskResultType;

/**
 * Holds some meta data for a delete task for some data set.<br/>
 * Knows how to print itself.
 *
 */
public class DataSetTaskMetaData {

	private long id;

	private String uuid;

	private String name;

	private String dsType;

	private TaskResultType resultType = TaskResultType.SUCCESS;

	public DataSetTaskMetaData(DataSet ds) {
		this.id = ds.getId().longValue();
		this.uuid = ds.getUuidAsString();
		this.name = ds.getName() != null ? ds.getName().getDefaultValue() : null;
		dsType = ds.getDataSetType() != null ? ds.getDataSetType().getValue() : null;
	}

	public DataSetTaskMetaData(DataSet ds, TaskResultType resultType) {
		this(ds);
		this.resultType = resultType;
	}

	@Override
	public String toString() {
		return getDsMetaString() + " --- Deletion: " + resultType.getValue() + ".";
	}

	public String getDsMetaString() {
		StringBuilder sb = new StringBuilder("");
		sb.append(dsType.toUpperCase() + ": ");
		sb.append("\"" + name + "\"");
		sb.append(" with UUID: " + uuid);
		sb.append(", ID: " + id);
		return sb.toString();
	}

	public long getId() {
		return id;
	}

	public String getUuid() {
		return uuid;
	}

	public String getName() {
		return name;
	}

	public String getDsType() {
		return dsType;
	}

	public TaskResultType getResultType() {
		return resultType;
	}

	public void setResultType(TaskResultType resultType) {
		this.resultType = resultType;
	}
}
