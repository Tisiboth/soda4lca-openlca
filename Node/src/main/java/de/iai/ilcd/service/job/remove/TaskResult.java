package de.iai.ilcd.service.job.remove;

import de.iai.ilcd.model.common.DataSet;
import de.iai.ilcd.service.job.TaskResultType;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Wraps delete task data for a data set and its dependencies.<br/>
 * Evaluates the overall result.<br/>
 * Knows how to print itself <br/>
 *
 */
public class TaskResult {

	private static final String LINE_SEPARATOR = System.getProperty("line.separator");

	private Long id;

	private DataSetTaskMetaData dsTask;

	private Set<DataSetTaskMetaData> dependencyTasks = new HashSet<DataSetTaskMetaData>();

	private TaskResultType overallResult = TaskResultType.SUCCESS;

	public TaskResult(DataSetTaskMetaData dsTask) {
		this.dsTask = dsTask;
		this.id = this.dsTask.getId();

		if (!TaskResultType.SUCCESS.equals(dsTask.getResultType())) {
			overallResult = dsTask.getResultType();
		}
	}

	public TaskResult(DataSet ds, TaskResultType type) {
		this(new DataSetTaskMetaData(ds, type));
	}

	public void addDependencyResult(DataSetTaskMetaData dependencyMeta) {
		dependencyTasks.add(dependencyMeta);
		if (overallResult.equals(TaskResultType.SUCCESS)
				&& TaskResultType.ERROR.equals(dependencyMeta.getResultType())) {
			overallResult = TaskResultType.PARTIALLY_SUCCESSFUL;
		}
	}

	public void setTaskResult(TaskResultType type) {
		dsTask.setResultType(type);
		overallResult = TaskResultType.merge(overallResult, type);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("");
		sb.append(dsTask.toString() + LINE_SEPARATOR);

		// Dependencies
		if (dependencyTasks.size() > 0) {
			// Successes
			String depString = this.printDependenciesOfType(TaskResultType.SUCCESS);
			sb.append(depString);

			// Errors
			depString = this.printDependenciesOfType(TaskResultType.ERROR);
			sb.append(depString);
		}

		return sb.toString();
	}

	private String printDependenciesOfType(TaskResultType type) {
		StringBuilder sb = new StringBuilder("");
		List<DataSetTaskMetaData> filteredMeta;

		sb.append("--- Dependency deletion: " + type.getValue().toUpperCase() + LINE_SEPARATOR);

		filteredMeta = dependencyTasks.stream().filter(ds -> type.equals(ds.getResultType()))
				.collect(Collectors.toList());

		if (filteredMeta.size() > 0) {
			for (DataSetTaskMetaData meta : filteredMeta)
				sb.append(">>>Dependency>>> " + meta.getDsMetaString() + LINE_SEPARATOR);
		} else {
			return "";
		}

		return sb.toString();
	}

	public Long getId() {
		return id;
	}

	public DataSetTaskMetaData getDsTask() {
		return dsTask;
	}

	public Set<DataSetTaskMetaData> getDependencyTasks() {
		return dependencyTasks;
	}

	public TaskResultType getOverallResult() {
		return overallResult;
	}
	
	public boolean is(TaskResultType resultType) {
		if (resultType == null)
			return false;
		
		return resultType.equals(overallResult);
	}
	
}
