package de.iai.ilcd.service;

import de.iai.ilcd.model.common.DataSet;
import de.iai.ilcd.model.dao.*;
import de.iai.ilcd.model.lciamethod.LCIAMethod;
import de.iai.ilcd.model.process.Process;
import de.iai.ilcd.model.security.User;
import de.iai.ilcd.service.exception.JobNotScheduledException;
import de.iai.ilcd.service.job.JobType;
import de.iai.ilcd.service.job.TaskResultType;
import de.iai.ilcd.service.job.remove.DataSetTaskMetaData;
import de.iai.ilcd.service.job.remove.DeleteWithDependenciesJob;
import de.iai.ilcd.service.job.remove.TaskResult;
import de.iai.ilcd.util.DependenciesUtil;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("deleteWithDependenciesService")
public class DeleteWithDependenciesService extends JobService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DeleteWithDependenciesService.class);
	
	public static final int LARGE_WORKLOAD = 15;
	
	public static final int LARGE_WORKLOAD_DEPENDENCY_NIGHTMARE = 3;

	public DeleteWithDependenciesService() throws SchedulerException {
		super();
	}
	
	/**
	 * @param dsArray
	 * @param depMode
	 * @param user
	 * @return
	 * 			List of task results. Null is returned if and only if a job has been scheduled.
	 * @throws JobNotScheduledException
	 */
	public List<TaskResult> delete(DataSet[] dsArray, DependenciesMode depMode, User user) throws JobNotScheduledException {
		if (LOGGER.isTraceEnabled())
			LOGGER.trace("Service has recieved deletion order of " + dsArray.length + " dataSets with DependenciesMode= " + depMode.name());

		if (dsArray == null || dsArray.length == 0) {
			LOGGER.error("Attempted deletion without any data sets.");
			return new ArrayList<TaskResult>();
		}
		boolean isDependencyNightmare = dsArray[0] instanceof Process || dsArray[0] instanceof LCIAMethod;
		int largeWorkload = isDependencyNightmare ? LARGE_WORKLOAD_DEPENDENCY_NIGHTMARE : LARGE_WORKLOAD;
		
		if (dsArray.length >= largeWorkload) {
			if (LOGGER.isTraceEnabled())
				LOGGER.trace("Data set count " + dsArray.length + " is considered large. Scheduling a job..");
			
			String jobDescription = "Deletion of " + dsArray.length + " data sets with DependenciesMode=" + depMode.name() + "."; 
			
			// Configures the job by storing whatever the job needs in an
			// specialized (String, Object)-map. Look up the AssignRemoveJob
			// class to find out which keys are necessary and how they are used.
			JobDataMap jobDataMap = new JobDataMap();
			jobDataMap.put(DeleteWithDependenciesJob.DATA_SETS_KEY, dsArray);
			jobDataMap.put(DeleteWithDependenciesJob.DEPENDENCIES_MODE_KEY, depMode);
			
			JobDetail job = JobBuilder.newJob(DeleteWithDependenciesJob.class).withIdentity(UUID.randomUUID().toString()).withDescription(jobDescription.toString())
			.usingJobData(jobDataMap).build();
			LOGGER.debug("Trying to queue detach job.");
			try {
				super.queueJob(job, user, JobType.DELETE);
				
			} catch (PersistException | MergeException | JobNotScheduledException e) {
				throw new JobNotScheduledException(e.getMessage());
			}
			
			// We're gonna flag the data sets for deletion in batches.
			Map<DataSetDaoType, Set<DataSet>> dsCollections = DataSetDaoType.groupByDaoType(Arrays.asList(dsArray));
			for (DataSetDaoType daoType : dsCollections.keySet())
				daoType.getDao().flagforDeletion(dsCollections.get(daoType));
			
			return null; // because a Job is now scheduled and responsible, we can't publish results here.
			
		} else {
			if (LOGGER.isTraceEnabled())
				LOGGER.trace("Data set count " + dsArray.length + " is considered small enough. Won't schedule a job.");
			
			List<TaskResult> results = new ArrayList<TaskResult>();
			for (DataSet ds : dsArray) {
				TaskResult taskResult = doDelete(ds, depMode);
				results.add(taskResult);
			}
			if (LOGGER.isDebugEnabled())
				LOGGER.debug("Finished deleting data sets.");
			
			return results;
		}
	}
	
	public static TaskResult doDelete(DataSet ds, DependenciesMode depMode) {
		
		DataSetTaskMetaData dsTaskMeta = new DataSetTaskMetaData(ds);
		TaskResult result = new TaskResult(dsTaskMeta);
		
		DataSetDao<?,?,?> dao = ds.getCorrespondingDSDao();
		
		try {
			// Dependencies first
			if (!DependenciesMode.NONE.equals(depMode)) {
				doDeleteDependencies(ds, depMode, result);
			}

			// now the data set
			dao.removeById(ds.getId());
		} catch (Exception e) {
			result.setTaskResult(TaskResultType.ERROR);

			LOGGER.error(dsTaskMeta.toString(), e);

		}
		
		return result;
	}

	private static void doDeleteDependencies(DataSet ds, DependenciesMode depMode, TaskResult result) throws Exception {

		Set<DataSet> deps = new HashSet<>();
		DependenciesUtil.crawlDependencies(deps, deps, false, 1);
		for (DataSet dep : deps) {
			try {
				DataSetDao<?, ?, ?> depDao = dep.getCorrespondingDSDao();
				depDao.removeById(dep.getId());
				result.addDependencyResult( new DataSetTaskMetaData(dep, TaskResultType.SUCCESS));
				
			} catch (Exception e) {
				DataSetTaskMetaData depTaskMeta = new DataSetTaskMetaData(dep, TaskResultType.ERROR);
				result.addDependencyResult( depTaskMeta);
				
				if (LOGGER.isTraceEnabled())
					LOGGER.trace(depTaskMeta.toString(), e);
			}
		}
	}

}
