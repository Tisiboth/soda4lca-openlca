package de.iai.ilcd.service.job.remove;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import de.iai.ilcd.model.common.DataSet;
import de.iai.ilcd.model.common.JobMetaData;
import de.iai.ilcd.model.dao.DependenciesMode;
import de.iai.ilcd.model.dao.JobMetaDataDao;
import de.iai.ilcd.model.dao.MergeException;
import de.iai.ilcd.service.DeleteWithDependenciesService;
import de.iai.ilcd.service.job.TaskResultType;

public class DeleteWithDependenciesJob implements Job {
	
	JobDataMap jobData;
	
	DeleteWithDependenciesLog log;
	
	public static final String DATA_SETS_KEY = "dataSetArray";
	
	public static final String DEPENDENCIES_MODE_KEY = "depMode";

	JobMetaData jmd;

	private JobMetaDataDao jmdDao;	

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		init(context);
		
		DataSet[] dataSets = (DataSet[]) jobData.get(DATA_SETS_KEY);
		DependenciesMode depMode = (DependenciesMode) jobData.get(DEPENDENCIES_MODE_KEY);
		
		for (DataSet ds : dataSets) {
			
			TaskResult taskResult = DeleteWithDependenciesService.doDelete(ds, depMode);
			updateJobMetaData(taskResult);
		}
	}

	private void init(JobExecutionContext context) {
		jmdDao = new JobMetaDataDao();
		jmd = jmdDao.getJobMetaData(context.getJobDetail().getKey().getName());
		jmd.setErrorsCount(new Long(0));
		jmd.setInfosCount(new Long(0));
		jmd.setSuccessesCount(new Long(0));
		
		log = new DeleteWithDependenciesLog();
		log.setJobDescription(context.getJobDetail().getDescription());
		
		jobData = context.getMergedJobDataMap();
	}
	
	
	private void updateJobMetaData(TaskResult taskResult) {
		
		log.logTaskResult(taskResult);
		
		// update counters
		TaskResultType resultType = taskResult.getOverallResult();
		if ( TaskResultType.ERROR.equals( resultType ) )
			countError();
		else if  ( TaskResultType.SUCCESS.equals( resultType ) )
			countSuccess();
		else if  ( TaskResultType.PARTIALLY_SUCCESSFUL.equals( resultType ) )
			countInfo();
		
		jmd.setLog(log.toString());
		
		try {
			jmdDao.merge(jmd);
		} catch (MergeException e) {
			e.printStackTrace();
		}
	}

	private void countSuccess() {
		Long l = jmd.getSuccessesCount();		
		l += 1;
		jmd.setSuccessesCount(l);
	}
	
	private void countError() {
		Long l = jmd.getErrorsCount();
		l += 1;
		jmd.setErrorsCount(l);
	}
	
	private void countInfo() {
		Long l = jmd.getInfosCount();
		l += 1;
		jmd.setInfosCount(l);
	}

}
