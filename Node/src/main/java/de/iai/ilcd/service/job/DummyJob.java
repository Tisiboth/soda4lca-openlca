package de.iai.ilcd.service.job;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class DummyJob implements Job {

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			// TODO Auto-generated method stub
			JobDataMap dataMap = context.getMergedJobDataMap();
			int sleep = dataMap.getInt("sleep");

			Thread.sleep(sleep);
		} catch (Exception e) {

			// e.printStackTrace();
			throw new JobExecutionException(e);
		}

	}

}
