package de.iai.ilcd.service.job;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import de.iai.ilcd.model.common.DataSet;
import de.iai.ilcd.model.common.GlobalReference;
import de.iai.ilcd.model.common.JobMetaData;
import de.iai.ilcd.model.dao.ContactDao;
import de.iai.ilcd.model.dao.DataSetDao;
import de.iai.ilcd.model.dao.ElementaryFlowDao;
import de.iai.ilcd.model.dao.FlowPropertyDao;
import de.iai.ilcd.model.dao.JobMetaDataDao;
import de.iai.ilcd.model.dao.LCIAMethodDao;
import de.iai.ilcd.model.dao.ProcessDao;
import de.iai.ilcd.model.dao.ProductFlowDao;
import de.iai.ilcd.model.dao.SourceDao;
import de.iai.ilcd.model.dao.UnitGroupDao;
import de.iai.ilcd.model.datastock.AbstractDataStock;
import de.iai.ilcd.model.datastock.IDataStockMetaData;
import de.iai.ilcd.service.AssignRemoveService;
import de.iai.ilcd.service.util.GlobalReferenceUtils;
import de.iai.ilcd.webgui.controller.admin.StockHandler;
import de.iai.ilcd.webgui.controller.admin.StockListHandler;

/**
 * A job for assigning and removing data sets.
 * 
 * @author alhajras
 *
 */
public class AssignRemoveJob implements Job {

	private final int pageSize = 100;

	/*
	 * The Assign/Remove Service that invoked this instance
	 */
	public static AssignRemoveService assignRemoveService;

	/*
	 * The unique ID of this job
	 */
	StringBuilder jobId;

	/*
	 * The job metadata of this job
	 */
	JobMetaData jmd;

	/*
	 * The log of all successes in job execution
	 */
	StringBuilder successesLog;

	/*
	 * The log of all infos during job execution
	 */
	StringBuilder infosLog;

	/*
	 * The log of all errors during job execution
	 */
	StringBuilder errorsLog;

	private StockListHandler stockListHandler = new StockListHandler();

	private ArrayList<DataSetsTypes> selectedDataSetTypes = new ArrayList<DataSetsTypes>();

	/*
	 * The mapping from uuid and version of each proccess to list of its
	 * dependencies
	 */
	Map<String, List<String>> processDependencies = new HashMap<String, List<String>>();

	/*
	 * The logger for logging data
	 */
	protected final Logger log = org.apache.log4j.Logger.getLogger(this.getClass());

	/**
	 * This Job assigns/removes data sets (or referenced data sets) to a stock.
	 * 
	 * How to set up the JobDataMap? Let's start with the most necessary keys:
	 * 
	 * Key: stockHandler		Value: StockHandler 	// The stock to to/from which data sets must be assigned/removed.
	 * Key: type				Value: JobType			// JobType.ATTACH or JobType.DETACH
	 * 
	 * Those two keys are always necessary.
	 * Now the rest depends on whether you already have the data sets or how the relevant data sets are referenced...
	 * 
	 * 
	 * 
	 * Case 1: You already have an array of <code>DataSet<code>.
	 * 
	 * Key: singleDatasetType				Value: (Boolean) true			// Just to access the relevant parts of the code.
 	 * Key: usingGlobalReferences			Value: (Boolean) false			// Just to access the relevant parts of the code.
 	 * Key: dataSetSelectableDataModel		Value: DataSetTypes[]			// The data sets to be assigned/removed.
 	 * 
	 * 
	 * 
	 * Case 2: You have an array of <code>DataSetsTypes<code> and want to fetch all data sets of those
	 * types from a selection of data stocks. 
	 * 
	 * Key: singleDatasetType				Value: (Boolean) false			// Just to access the relevant parts of the code.
 	 * Key: usingGlobalReferences			Value: (Boolean) false			// Just to access the relevant parts of the code.
 	 * Key: selectedDataSetTypes			Value: DataSetTypes[]			// The data set types we care for.
 	 * Key: stockListHandler				Value: StockListHandler			// The stockListHandler keeping track of the selected stocks for fetching the datasets.
 	 * 
 	 * 
 	 * 
 	 * Case 3: You have an set of <code>GlobalReference<code> and want to fetch all referenced data sets of those 
 	 * from a selection of data stocks.
	 * 
	 * Key: singleDatasetType				Value: (Boolean) false			// Just to access the relevant parts of the code.
 	 * Key: usingGlobalReferences			Value: (Boolean) true			// Just to access the relevant parts of the code.
 	 * Key: references						Value: Set<GlobalReference>		// The references for data sets we care for.
 	 * Key: stocksToFetchFrom				Value: AbstractDataStock[]		// If we want to restrict fetching the referenced
 	 * 																		// data sets to certain stocks, else use <code>null<code>
 	 * 
	 */
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			// Preparations
			JobMetaDataDao jobMetaDataDao = new JobMetaDataDao();
			jmd = jobMetaDataDao.getJobMetaData(context.getJobDetail().getKey().getName());
			jmd.setErrorsCount(new Long(0));
			jmd.setInfosCount(new Long(0));
			jmd.setSuccessesCount(new Long(0));
			successesLog = new StringBuilder("SUCCESS\n");
			infosLog = new StringBuilder("INFO\n");
			errorsLog = new StringBuilder("ERROR\n");
			
			// Find out what needs to be done.
			JobDataMap jobDataMap = context.getMergedJobDataMap();
			StockHandler stockHandler = (StockHandler) jobDataMap.get("stockHandler");
			JobType jobType = (JobType) jobDataMap.get("type");
			jobId = new StringBuilder(context.getJobDetail().getKey().getName());
			boolean singleDatasetTyp = (boolean) jobDataMap.get("singleDatasetType");
			boolean usingGlobalReferences = (boolean) jobDataMap.get("usingGlobalReferences");

			if (usingGlobalReferences) {
				//Retrieve infos from map
				@SuppressWarnings("unchecked")
				Set<GlobalReference> references = (Set<GlobalReference>) jobDataMap.get("references");
				AbstractDataStock[] sourceStocks = (AbstractDataStock[]) jobDataMap.get("stocksToFetchFrom");
				
				//fetch referenced data sets from the given stocks
				DataSet[] dataSetsSelected = GlobalReferenceUtils.getReferencedDataSetsFromStocks(references,
						sourceStocks);
				
				//Some references may remain unresolved. We want the user to at least know how many.
				long numberOfUnavailableDataSets = (long) references.size() - dataSetsSelected.length;
				jmd.setInfosCount(numberOfUnavailableDataSets);
				infosLog.append(numberOfUnavailableDataSets
						+ " of the referenced data sets were not found in the selected stocks."
						+ System.lineSeparator());
				
				//now we can attach/detach
				attachDetachDataStockService(stockHandler, singleDatasetTyp, jobType, dataSetsSelected, usingGlobalReferences);
			} else if (singleDatasetTyp) {
				DataSet[] datasetsSelected = (DataSet[]) jobDataMap.get("dataSetSelectableDataModel");
				attachDetachDataStockService(stockHandler, singleDatasetTyp, jobType, datasetsSelected, usingGlobalReferences);
			} else {
				stockListHandler = (StockListHandler) jobDataMap.get("stockListHandler");
				Collections.addAll(selectedDataSetTypes, (DataSetsTypes[]) jobDataMap.get("selectedDataSetTypes"));
				attachDetachDataStockService(stockHandler, singleDatasetTyp, jobType, new DataSet[0], usingGlobalReferences);
			}

		} catch (Exception e) {
			log.warn("Assign-Remove job could not be executed correctly!");
			if (log.isDebugEnabled()) {
				e.printStackTrace();
			}
			throw new JobExecutionException(e);
		}
	}

	/**
	 * Attach/Detach datasets to data stock.
	 * 
	 * @param stockHandler     The password for authorizing target node
	 * @param jobType
	 * @param singleDatasetTyp
	 * @param datasetsSelected
	 * @param usingGlobalReferences
	 */
	protected void attachDetachDataStockService(StockHandler stockHandler, boolean singleDatasetTyp, JobType jobType,
			DataSet[] datasetsSelected, boolean usingGlobalReferences) {
		long start = 0;
		if (log.isDebugEnabled()) {
			start = System.currentTimeMillis();
		}

		long dsCounts = 0;

		if (singleDatasetTyp || usingGlobalReferences) {
			dsCounts = datasetsSelected.length;
		} else {
			dsCounts = getCountOfAllDatasets(stockHandler);
		}

		String toFrom = jobType == JobType.ATTACH ? "to " : "from "; 
		
		jmd.setJobName(
				jobType.getValue() + " " + dsCounts + " datasets " + toFrom + stockHandler.getEntry().getName());
		stockHandler.getProcessWrapper().getJobLoggerSrevice().setMetaData(log, jmd, successesLog, infosLog, errorsLog, jobId);

		if (singleDatasetTyp || usingGlobalReferences) {
			long pages = calculatePages(dsCounts);
			DataSet[] dataSetArray = new DataSet[pageSize];
			
			//Because the number of data sets may be large, we do the work in batches.
			int numberOfRemainingDataSets = (int) dsCounts;
			int position = 0;
			boolean isLastBatch = numberOfRemainingDataSets <= pageSize; //After attaching/detaching the last batch, the dependencyMode needs to be reset, which is why we need this flag.
			
			for (int i = 0; i < pages; i++) {
				position = i*pageSize;
				if (isLastBatch) { // The last batch must be handled carefully.
					dataSetArray = Arrays.copyOfRange(datasetsSelected, position, (position + numberOfRemainingDataSets) );
				} else {
					dataSetArray = Arrays.copyOfRange(datasetsSelected, position, (position + pageSize) );
				}
				if (jobType == JobType.ATTACH)
					stockHandler.getProcessWrapper().attachToStock(stockHandler.getEntry(), dataSetArray, false, isLastBatch); //If we're attaching the last batch, we want to reset the dependencyMode afterwards.
				else if (jobType == JobType.DETACH) {
					stockHandler.getProcessWrapper().detachFromStock(stockHandler.getEntry(), dataSetArray, false, isLastBatch); //If we're detaching the last batch, we want to reset the dependencyMode afterwards.
				}
				numberOfRemainingDataSets -= dataSetArray.length;
				isLastBatch = numberOfRemainingDataSets < pageSize;
			}
		} else {
			AbstractDataStock[] list = stockListHandler.getSelectedStocks();
			for (AbstractDataStock abstractDataStock : list) {
				if (selectedDataSetTypes.contains(DataSetsTypes.PROCESSES)) {
					ProcessDao dao = new ProcessDao();
					startBatching(dao, abstractDataStock, stockHandler, jobType);
				}
				if (selectedDataSetTypes.contains(DataSetsTypes.ELEMENTARY_FLOWS)) {
					ElementaryFlowDao dao = new ElementaryFlowDao();
					startBatching(dao, abstractDataStock, stockHandler, jobType);
				}
				if (selectedDataSetTypes.contains(DataSetsTypes.FLOW_PROPERTIES)) {
					FlowPropertyDao dao = new FlowPropertyDao();
					startBatching(dao, abstractDataStock, stockHandler, jobType);
				}
				if (selectedDataSetTypes.contains(DataSetsTypes.LCIA_METHODS)) {
					LCIAMethodDao dao = new LCIAMethodDao();
					startBatching(dao, abstractDataStock, stockHandler, jobType);
				}
				if (selectedDataSetTypes.contains(DataSetsTypes.PRODUCT_FLOWS)) {
					ProductFlowDao dao = new ProductFlowDao();
					startBatching(dao, abstractDataStock, stockHandler, jobType);
				}
				if (selectedDataSetTypes.contains(DataSetsTypes.SOURCES)) {
					SourceDao dao = new SourceDao();
					startBatching(dao, abstractDataStock, stockHandler, jobType);
				}
				if (selectedDataSetTypes.contains(DataSetsTypes.UNIT_GROUPS)) {
					UnitGroupDao dao = new UnitGroupDao();
					startBatching(dao, abstractDataStock, stockHandler, jobType);
				}
				if (selectedDataSetTypes.contains(DataSetsTypes.CONTACTS)) {
					ContactDao dao = new ContactDao();
					startBatching(dao, abstractDataStock, stockHandler, jobType);
				}
			}
		}

		if (log.isDebugEnabled()) {
			long stop = System.currentTimeMillis();
			long duration = stop - start;
			log.debug("Job took " + (duration / 3600000) + " hours, " + ((duration % 3600000) / 60000) + " minutes, "
					+ ((duration % 60000) / 1000) + " seconds and " + (duration % 1000) + " milliseconds to complete.");
		}

		jmd.setLog("");

		if (!errorsLog.toString().equals("ERROR\n"))
			jmd.appendLog(errorsLog.toString());
		if (!successesLog.toString().equals("SUCCESS\n"))
			jmd.appendLog(successesLog.toString());
		if (!infosLog.toString().equals("INFO\n"))
			jmd.appendLog(infosLog.toString());

	}

	/**
	 * Method to start the assign/remove process.
	 * 
	 * @param DataSetDao        dao of the datset to process
	 * @param AbstractDataStock abstractDataStock is the target stock
	 * @param StockHandler      stockHandler The stockHandler target
	 * @param JobType           jobType either DETACH or ATTACH.
	 */
	@SuppressWarnings("unchecked")
	protected void startBatching(@SuppressWarnings("rawtypes") DataSetDao dao, AbstractDataStock abstractDataStock,
			StockHandler stockHandler, JobType jobType) {
		DataSet[] dataSetArray = new DataSet[pageSize];
		IDataStockMetaData[] stocks = new IDataStockMetaData[1];
		stocks[0] = abstractDataStock;

		long dataSetCount = dao.getCount(abstractDataStock);
		long pages = calculatePages(dataSetCount);
		boolean isLastBatch = false; //After attaching/detaching the last batch, the dependencyMode needs to be reset, which is why we need this flag.

		for (int i = 0; i < pages; i++) {
			if (((i * pageSize) + pageSize) >= dataSetCount) {
				int lastBatch = (int) (dataSetCount - (int) (((i * pageSize))));
				dataSetArray = new DataSet[lastBatch];
				isLastBatch = true;
			}

			(dao.getDataSets(stocks, null, true, (i * pageSize), pageSize)).toArray(dataSetArray);

			if (jobType == JobType.ATTACH)
				stockHandler.getProcessWrapper().attachToStock(stockHandler.getEntry(), dataSetArray, false, isLastBatch);
			else if (jobType == JobType.DETACH) {
				stockHandler.getProcessWrapper().detachFromStock(stockHandler.getEntry(), dataSetArray, false, isLastBatch);
			}
		}
	}

	/**
	 * determine in how many pages an export operation will be divided
	 * 
	 * @param dataSetCount
	 * @return
	 */
	private long calculatePages(long dataSetCount) {
		long pages = dataSetCount / pageSize;
		long remainder = dataSetCount % pageSize;
		if (remainder > 0) {
			pages++;
		}
		return pages;
	}

	/**
	 * Method to calculate the amount of datasets to be removed or assigned in the
	 * job.
	 * 
	 * @param stockHandler The stockHandler target
	 */
	private long getCountOfAllDatasets(StockHandler stockHandler) {
		AbstractDataStock[] list = stockListHandler.getSelectedStocks();
		IDataStockMetaData[] stocks = new IDataStockMetaData[1];
		long dataSetCount = 0;

		for (AbstractDataStock abstractDataStock : list) {
			stocks[0] = abstractDataStock;
			if (selectedDataSetTypes.contains(DataSetsTypes.PROCESSES)) {
				ProcessDao dao = new ProcessDao();
				dataSetCount += dao.getCount(abstractDataStock);
			}
			if (selectedDataSetTypes.contains(DataSetsTypes.ELEMENTARY_FLOWS)) {
				ElementaryFlowDao dao = new ElementaryFlowDao();
				dataSetCount += dao.getCount(abstractDataStock);
			}
			if (selectedDataSetTypes.contains(DataSetsTypes.FLOW_PROPERTIES)) {
				FlowPropertyDao dao = new FlowPropertyDao();
				dataSetCount += dao.getCount(abstractDataStock);
			}
			if (selectedDataSetTypes.contains(DataSetsTypes.LCIA_METHODS)) {
				LCIAMethodDao dao = new LCIAMethodDao();
				dataSetCount += dao.getCount(abstractDataStock);
			}
			if (selectedDataSetTypes.contains(DataSetsTypes.PRODUCT_FLOWS)) {
				ProductFlowDao dao = new ProductFlowDao();
				dataSetCount += dao.getCount(abstractDataStock);
			}
			if (selectedDataSetTypes.contains(DataSetsTypes.SOURCES)) {
				SourceDao dao = new SourceDao();
				dataSetCount += dao.getCount(abstractDataStock);
			}
			if (selectedDataSetTypes.contains(DataSetsTypes.UNIT_GROUPS)) {
				UnitGroupDao dao = new UnitGroupDao();
				dataSetCount += dao.getCount(abstractDataStock);
			}
			if (selectedDataSetTypes.contains(DataSetsTypes.CONTACTS)) {
				ContactDao dao = new ContactDao();
				dataSetCount += dao.getCount(abstractDataStock);
			}

		}
		return dataSetCount;
	}

}
