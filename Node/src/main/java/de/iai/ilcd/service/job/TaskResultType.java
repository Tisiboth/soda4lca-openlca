package de.iai.ilcd.service.job;

/**
 * Result types, ordered: good < bad.
 *
 */
public enum TaskResultType {
	
	SUCCESS("Successful", "Success"),
	PARTIALLY_SUCCESSFUL("Partially succesful", "PartialSuccess"),
	ERROR("Error", "Error");

	private String value;
	
	private String suggestedSuffix;

	TaskResultType(String s, String key) {
		value = s;
		suggestedSuffix = key;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public String asSuffix() {
		return this.suggestedSuffix;
	}
	
	/**
	 * Decides which of the ResultTypes is worse.
	 * 
	 * @param t
	 * @param s
	 * @return
	 * 			merged result type
	 */
	public static TaskResultType merge(TaskResultType t, TaskResultType s) {
		
		if (t.compareTo(s) < 0)
			return s;
		
		// else
		return t;
	}
}
