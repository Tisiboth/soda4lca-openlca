package de.iai.ilcd.service.job.remove;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import de.iai.ilcd.model.common.DataSet;
import de.iai.ilcd.service.job.TaskResultType;


public class DeleteWithDependenciesLog {
	
	Map<Long, TaskResult> identifiedResults = new HashMap<Long, TaskResult>();
	
	private static final String LINE_SEPARATOR = System.getProperty("line.separator");
	
	String jobDescription;
	
	public DeleteWithDependenciesLog() {
		super();
	}
	
	public void logDataSetResult(DataSet ds, TaskResultType type) {
		if (ds != null &&  type != null) {
			DataSetTaskMetaData meta = new DataSetTaskMetaData(ds);
			meta.setResultType(type);
			TaskResult taskResult = new TaskResult( meta);
			identifiedResults.put(ds.getId(), taskResult);
		}
	}
	
	public void logTaskResult(TaskResult result) {
		this.identifiedResults.put(result.getId(), result);
	}
	
	public void logDependencyResult(Long dsId, DataSet dependency, TaskResultType type) {
		if(dependency != null && type != null) {
			
			TaskResult task = identifiedResults.get(dsId);
			if (task != null ) {
				DataSetTaskMetaData depMeta = new DataSetTaskMetaData(dependency);
				depMeta.setResultType(type);
				task.addDependencyResult(depMeta);
			}
		}
	}
	
	public Map<Long, TaskResult> getIdentifiedResults() {
		return identifiedResults;
	}
	
	@Override
	public String toString() {		
		StringBuilder sb = new StringBuilder();
		
		sb.append("### Job Description ###" + LINE_SEPARATOR);
		sb.append(jobDescription + LINE_SEPARATOR);
		sb.append("###" + LINE_SEPARATOR);
		
		// Successes
		sb.append(printAllOfType(TaskResultType.SUCCESS));
		sb.append(LINE_SEPARATOR);
		
		// Partially successful
		sb.append(printAllOfType(TaskResultType.PARTIALLY_SUCCESSFUL));
		sb.append(LINE_SEPARATOR);
		
		// Errors
		sb.append(printAllOfType(TaskResultType.ERROR));
		
		return sb.toString();
	}
	
	private String printAllOfType(TaskResultType type) {
		if (type == null)
			return null;
		
		StringBuilder sb = new StringBuilder("");
		sb.append("------- " + type.getValue().toUpperCase() + " ------" + LINE_SEPARATOR);
		
		List<TaskResult> filteredResults = identifiedResults.values().stream()
				.filter(res -> type.equals(res.getOverallResult())).collect(Collectors.toList());
		
		if ( filteredResults.size() > 0) {
			for (TaskResult result : filteredResults)
				sb.append(result.toString() + LINE_SEPARATOR);
			
		} else {
			sb.append("None." + LINE_SEPARATOR);
		}
		
		return sb.toString();
	}
	
	public void setJobDescription(String description) {
		this.jobDescription = description;
	}
}
