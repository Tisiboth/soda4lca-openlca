package de.iai.ilcd.rest.util;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import de.fzk.iai.ilcd.service.client.impl.vo.IntegerList;
import de.fzk.iai.ilcd.service.client.impl.vo.StringList;

/**
 * Serializer for string lists.
 */
public class JSONIntegerListSerializer implements JsonSerializer<IntegerList> {

	/**
	 * Private constructor, use {@link #register(GsonBuilder)}
	 */
	private JSONIntegerListSerializer() {
	}

	/**
	 * Register serializer in provided builder. Serialization of a list
	 * must be performed by passing an instance of the wrapper class {@link StringList} to {@link Gson}.
	 * 
	 * @param gsonBuilder
	 *            GSON builder
	 */
	public static void register( GsonBuilder gsonBuilder ) {
		gsonBuilder.registerTypeAdapter( IntegerList.class, new JSONIntegerListSerializer() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JsonElement serialize(IntegerList src, Type type, JsonSerializationContext context ) {
		JsonObject list = new JsonObject();

		JsonArray data = new JsonArray();
		for ( Integer i : src.getIntegers() ) {
			data.add( i );
		}

		list.add( src.getIdentifier(), data );

		return list;
	}

}
