package de.iai.ilcd.util;

import javax.faces.context.FacesContext;
import java.util.Locale;
import java.util.ResourceBundle;

public class SodaEpdResourceBundle extends SodaResourceBundle {

    /**
     * Name of the bundle
     */
    protected static final String BUNDLE_NAME = "resources.datasetdetail-epd";

    /**
     * Create the bundle
     */
    public SodaEpdResourceBundle() {
        this.setParent( ResourceBundle.getBundle( BUNDLE_NAME, FacesContext.getCurrentInstance().getViewRoot().getLocale(), SodaResourceBundle.UTF8_CONTROL ) );
    }

    public SodaEpdResourceBundle(String lang) {
        this.setParent( ResourceBundle.getBundle( BUNDLE_NAME, new Locale(lang), SodaResourceBundle.UTF8_CONTROL ) );
    }

}
