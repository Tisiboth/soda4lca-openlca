package de.iai.ilcd.util;

import javax.faces.context.FacesContext;
import java.util.Locale;
import java.util.ResourceBundle;

public class SodaTooltipResourceBundle extends SodaResourceBundle {

    /**
     * Name of the bundle
     */
    protected static final String BUNDLE_NAME = "resources.datasetdetail-tooltips";

    /**
     * Create the bundle
     */
    public SodaTooltipResourceBundle() {
        this.setParent( ResourceBundle.getBundle( BUNDLE_NAME, FacesContext.getCurrentInstance().getViewRoot().getLocale(), SodaResourceBundle.UTF8_CONTROL ) );
    }

    public SodaTooltipResourceBundle(String lang) {
        this.setParent( ResourceBundle.getBundle( BUNDLE_NAME, new Locale(lang), SodaResourceBundle.UTF8_CONTROL ) );
    }

}
