package de.iai.ilcd.util;

import de.iai.ilcd.model.common.DataSet;
import de.iai.ilcd.model.dao.DataSetDao;
import de.iai.ilcd.model.dao.DependenciesMode;
import de.iai.ilcd.model.process.Process;

import java.util.HashSet;
import java.util.Set;

public class DependenciesUtil {

	public Set<DataSet> getDependencies(DataSet dataset, DependenciesMode dependencyOptions) {
		DataSetDao<?, ?, ?> dao = DataSetDao.getDao(dataset);
		Set<DataSet> deps = dao.getDependencies(dataset, dependencyOptions);
		return deps;
	}

	/**
	 * first deps (accumulator) final set of dependencies to be added.
	 * seconds deps (child_dependencies) given the first layer of dependencies from ProcessDao
	 *
	 * Default depth is 2:
	 *
	 *		   a
	 *		    ├── b
	 *		    │   ├── d
	 *		    │   └── e
	 *		    └── c
	 *		        ├── f
	 *		        └── g
	 *
	 */
	public static void crawlDependencies(Set<DataSet> accumulator, Set<DataSet> child_dependencies, boolean ignoreProcess,
										 int depth, DependenciesMode depMode) {

		if (depth == 0) {
			// Mitigate any possible NPE
			accumulator.remove(null);
			return;
		}

		Set<DataSet> erd = new HashSet<DataSet>(), tmp;
		for (DataSet ds : child_dependencies) {
			if ((ds == null) || (ignoreProcess && ds instanceof Process))
				continue;
			DataSetDao<?, ?, ?> dao = ds.getCorrespondingDSDao();
			tmp = dao.getDependencies(ds, depMode);

			// replace this if Streams matured enough
			if (tmp != null)
				for (DataSet d : tmp)
					if ((d != null) && (!ignoreProcess || !(d instanceof Process)))
						erd.add(d);
//						accumulator.add(d); // ConcurrentModificationException??!
		}
		accumulator.addAll(erd);
		crawlDependencies(accumulator, erd, ignoreProcess, depth - 1);
	}

	public static void crawlDependencies(Set<DataSet> accumulator, Set<DataSet> child_dependencies, boolean ignoreProcess,
										 int depth) {

		crawlDependencies(accumulator, child_dependencies, ignoreProcess, depth, DependenciesMode.ALL_FROM_DATASTOCK);
	}
}
