package de.iai.ilcd.webgui.controller.util;

import java.io.IOException;
import java.io.Writer;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import de.fzk.iai.ilcd.service.model.common.IClass;
import de.fzk.iai.ilcd.service.model.process.IComplianceSystem;
import de.iai.ilcd.configuration.ConfigurationService;
import de.iai.ilcd.model.common.DataSetType;
import de.iai.ilcd.model.common.GlobalReference;
import de.iai.ilcd.model.dao.ProcessDao;
import de.iai.ilcd.model.datastock.ExportType;
import de.iai.ilcd.model.flow.MaterialProperty;
import de.iai.ilcd.model.flow.ProductFlow;
import de.iai.ilcd.model.process.Process;
import de.iai.ilcd.model.utils.DataSetUUIDVersionComparator;
import de.iai.ilcd.util.CategoryTranslator;

public class CSVFormatter {

	private static DecimalSeparator decimalSeparator = DecimalSeparator.DOT;
	private static String entrySeparator = ";";

	protected enum indicatorsHeaderEnum {
		GWP("77e416eb-a363-4258-a04e-171d843a6460", IndicatorsValueType.LCIAResult),
		ODP("06dcd26f-025f-401a-a7c1-5e457eb54637", IndicatorsValueType.LCIAResult),
		POCP("1e84a202-dae6-42aa-9e9d-71ea48b8be00", IndicatorsValueType.LCIAResult),
		AP("b4274add-93b7-4905-a5e4-2e878c4e4216", IndicatorsValueType.LCIAResult),
		EP("f58827d0-b407-4ec6-be75-8b69efb98a0f", IndicatorsValueType.LCIAResult),
		ADPE("f7c73bb9-ab1a-4249-9c6d-379a0de6f67e", IndicatorsValueType.LCIAResult),
		ADPF("804ebcdf-309d-4098-8ed8-fdaf2f389981", IndicatorsValueType.LCIAResult),

		PERE("20f32be5-0398-4288-9b6d-accddd195317", IndicatorsValueType.Exchange),
		PERM("fb3ec0de-548d-4508-aea5-00b73bf6f702", IndicatorsValueType.Exchange),
		PERT("53f97275-fa8a-4cdd-9024-65936002acd0", IndicatorsValueType.Exchange),
		PENRE("ac857178-2b45-46ec-892a-a9a4332f0372", IndicatorsValueType.Exchange),
		PENRM("1421caa0-679d-4bf4-b282-0eb850ccae27", IndicatorsValueType.Exchange),
		PENRT("06159210-646b-4c8d-8583-da9b3b95a6c1", IndicatorsValueType.Exchange),
		SM("c6a1f35f-2d09-4f54-8dfb-97e502e1ce92", IndicatorsValueType.Exchange),
		RSF("64333088-a55f-4aa2-9a31-c10b07816787", IndicatorsValueType.Exchange),
		NRSF("89def144-d39a-4287-b86f-efde453ddcb2", IndicatorsValueType.Exchange),
		FW("3cf952c8-f3a4-461d-8c96-96456ca62246", IndicatorsValueType.Exchange),
		HWD("430f9e0f-59b2-46a0-8e0d-55e0e84948fc", IndicatorsValueType.Exchange),
		NHWD("b29ef66b-e286-4afa-949f-62f1a7b4d7fa", IndicatorsValueType.Exchange),
		RWD("3449546e-52ad-4b39-b809-9fb77cea8ff6", IndicatorsValueType.Exchange),
		CRU("a2b32f97-3fc7-4af2-b209-525bc6426f33", IndicatorsValueType.Exchange),
		MFR("d7fe48a5-4103-49c8-9aae-b0b5dfdbd6ae", IndicatorsValueType.Exchange),
		MER("59a9181c-3aaf-46ee-8b13-2b3723b6e447", IndicatorsValueType.Exchange),
		EEE("4da0c987-2b76-40d6-9e9e-82a017aaaf29", IndicatorsValueType.Exchange),
		EET("98daf38a-7a79-46d3-9a37-2b7bd0955810", IndicatorsValueType.Exchange),
		
		A2AP("b5c611c6-def3-11e6-bf01-fe55135034f3", IndicatorsValueType.LCIAResult),
		A2GWPtotal("6a37f984-a4b3-458a-a20a-64418c145fa2", IndicatorsValueType.LCIAResult),
		A2GWPbiogenic("2356e1ab-0185-4db5-86e5-16de51c7485c", IndicatorsValueType.LCIAResult),
		A2GWPfossil("5f635281-343e-44fb-83df-1971b155e6b6", IndicatorsValueType.LCIAResult),
		A2GWPluluc("4331bbdb-978a-490d-8707-eeb047f01a55", IndicatorsValueType.LCIAResult),
		A2ETPfw("ee1082d1-b0f7-43ca-a1f0-21e2a4a74511", IndicatorsValueType.LCIAResult),
		A2PM("b5c602c6-def3-11e6-bf01-fe55135034f3", IndicatorsValueType.LCIAResult),
		A2EPmarine("b5c619fa-def3-11e6-bf01-fe55135034f3", IndicatorsValueType.LCIAResult),
		A2EPfreshwater("b5c619fa-def3-11e6-bf01-fe55135034f3", IndicatorsValueType.LCIAResult),
		A2EPterrestrial("b5c614d2-def3-11e6-bf01-fe55135034f3", IndicatorsValueType.LCIAResult),
		A2HTPc("2299222a-bbd8-474f-9d4f-4dd1f18aea7c", IndicatorsValueType.LCIAResult),
		A2HTPnc("3af763a5-b7a1-48c9-9cee-1f223481fcef", IndicatorsValueType.LCIAResult),
		A2IRP("b5c632be-def3-11e6-bf01-fe55135034f3", IndicatorsValueType.LCIAResult),
		A2SOP("b2ad6890-c78d-11e6-9d9d-cec0c932ce01", IndicatorsValueType.LCIAResult),
		A2ODP("b5c629d6-def3-11e6-bf01-fe55135034f3", IndicatorsValueType.LCIAResult),
		A2POCP("b5c610fe-def3-11e6-bf01-fe55135034f3", IndicatorsValueType.LCIAResult),
		A2ADPF("b2ad6110-c78d-11e6-9d9d-cec0c932ce01", IndicatorsValueType.LCIAResult),
		A2ADPE("b2ad6494-c78d-11e6-9d9d-cec0c932ce01", IndicatorsValueType.LCIAResult),
		A2WDP("b2ad66ce-c78d-11e6-9d9d-cec0c932ce01", IndicatorsValueType.LCIAResult);

		String UUID;
		IndicatorsValueType vt;

		indicatorsHeaderEnum(String UUID, IndicatorsValueType vt) {
			this.UUID = UUID;
			this.vt = vt;
		}

		String getResultValue(Process process, String module) {
			switch (vt) {
			case LCIAResult:
				return getLciaResultValue(process, UUID, module);
			case Exchange:
				return getExchangeValue(process, UUID, module);
			default:
				return "MISSING IndicatorsValueType";
			}

		}

		private String getExchangeValue(Process process, String indicatorUUID, String module) {
			try {
				return format(
						process.getExchangesByIndicator(indicatorUUID).get(0).getAmountByModule(module).getValue());
			} catch (Exception e) {
				return "";
			}
		}

		private String getLciaResultValue(Process process, String indicatorUUID, String module) {
			try {
				return format(
						process.getLciaResultsByIndicator(indicatorUUID).get(0).getAmountByModule(module).getValue());
			} catch (Exception e) {
				return "";
			}
		}
		
		private boolean isA2() {
			// Additional EN15804 A2 indicators uuids
			List<String> A2Ids = Stream.of("ee1082d1-b0f7-43ca-a1f0-21e2a4a74511",
					"b5c611c6-def3-11e6-bf01-fe55135034f3",
					"6a37f984-a4b3-458a-a20a-64418c145fa2",
					"2356e1ab-0185-4db5-86e5-16de51c7485c",
					"5f635281-343e-44fb-83df-1971b155e6b6",
					"4331bbdb-978a-490d-8707-eeb047f01a55",
					"ee1082d1-b0f7-43ca-a1f0-21e2a4a74511",
					"b5c602c6-def3-11e6-bf01-fe55135034f3",
					"b5c619fa-def3-11e6-bf01-fe55135034f3",
					"b5c619fa-def3-11e6-bf01-fe55135034f3",
					"b5c614d2-def3-11e6-bf01-fe55135034f3",
					"2299222a-bbd8-474f-9d4f-4dd1f18aea7c",
					"3af763a5-b7a1-48c9-9cee-1f223481fcef",
					"b5c632be-def3-11e6-bf01-fe55135034f3",
					"b2ad6890-c78d-11e6-9d9d-cec0c932ce01",
					"b5c629d6-def3-11e6-bf01-fe55135034f3",
					"b5c610fe-def3-11e6-bf01-fe55135034f3",
					"b2ad6110-c78d-11e6-9d9d-cec0c932ce01",
					"b2ad6494-c78d-11e6-9d9d-cec0c932ce01",
					"b2ad66ce-c78d-11e6-9d9d-cec0c932ce01")
					.collect(Collectors.toList());
			return A2Ids.contains(this.UUID);
		}

		@Override
		public String toString() {
			String s = this.name();
			
			// Format A2 indicators
			if(this.isA2()) {
				// Remove 'A2' prefix
				s.replaceFirst("A2", "");
				
				// Add Label at the end
				s += " (A2)";
			}
			return s;
		}

		enum IndicatorsValueType {
			LCIAResult, Exchange;
		};

	}

	protected enum HeaderEnum {
		UUID("UUID", "UUID"),
		Version("Version", "Version"),
		Name("Name", "Name"),
		Category("Category", "Kategorie"),
		Compliance("Compliance", "Konformität"),
		//<new>
		LocationCode("Location code", "Laenderkennung"),
		// </new>
		Type("Type", "Typ"),
		// <new>
		ReferenceYear("Reference year", "Referenzjahr"),
		ValidUntil("Valid until", "Gueltig bis"),
		URL("URL", "URL"),
		DeclarationOwner("Declaration owner", "Declaration owner"),
		PublicationDate("Publication date", "Veroeffentlicht am"),//1.2
		RegistrationNumber("Registration number", "Registrierungsnummer"),//1.2
		RegistrationAuthority("Registration authority", "Registrierungsstelle"),//1.2
		PredecessorUUID("Predecessor UUID", "UUID des Vorgängers"),//1.2
		PredecessorVersion("Predecessor Version", "Version des Vorgängers"),//1.2
		PredecessorURL("Predecessor URL", "URL des Vorgängers"), //1.2
		// </new>
		Refquantity("Ref. quantity", "Bezugsgroesse"),
		Refunit("Ref. unit", "Bezugseinheit"),
		ReferenceflowUUID("Reference flow UUID", "Referenzfluss-UUID"),
		Referenceflowname("Reference flow name", "Referenzfluss-Name"),
		Bulkdensity_kgm3("Bulk Density (kg/m3)", "Schuettdichte (kg/m3)"),
		Grammage_kgm2("Grammage (kg/m2)", "Flaechengewicht (kg/m2)"),
		Grossdensity_kgm3("Gross Density (kg/m3)", "Rohdichte (kg/m3)"),
		Layerthickness_m("Layer Thickness (m)", "Schichtdicke (m)"),
		Productiveness_m2("Productiveness (m2)", "Ergiebigkeit (m2)"),
		Lineardensity_kgm("Linear Density (kg/m)", "Laengengewicht (kg/m)"),
		Conversionfactorto1kg("Conversion factor to 1kg", "Umrechungsfaktor auf 1kg"),
		Module("Module", "Modul");

		String title_en, title_de;

		private HeaderEnum(String en, String de) {
			this.title_en = en;
			this.title_de = de;
		}
	}

	public CSVFormatter(DecimalSeparator decimalSeparator) {
		CSVFormatter.decimalSeparator = decimalSeparator;
	}

	public void writeHeader(Writer w) throws IOException {

		Function<HeaderEnum, String> langMapper;
		if ("de".equals(ConfigurationService.INSTANCE.getDefaultLanguage()))
			langMapper = h -> h.title_de;
		else
			langMapper = h -> h.title_en;

		for (HeaderEnum h : HeaderEnum.values()) {
			if (HeaderEnum.Name.equals(h)) {
				// We want to export the name in all preferred languages, with headers like: "Name (en)".
				for(String lang : ConfigurationService.INSTANCE.getPreferredLanguages()) {
					String s = langMapper.apply(h) + " (" + lang + ")";
					w.write(s + entrySeparator);
				}
			} else if (HeaderEnum.Category.equals(h)) {
				// Standard
				String std = langMapper.apply(h) + " (original)";
				w.write(std + entrySeparator);
				
				// Translated
				ConfigurationService.INSTANCE.getTranslateClassificationCSVlanguages().stream()
					.map(lang -> langMapper.apply(h) + " (" + lang + ")")
					.forEach(header -> {
						try {
							w.write(header + entrySeparator);
						} catch (IOException e) {
							e.printStackTrace();
						}
					});
			} else {
				String s = langMapper.apply(h);
				w.write(s + entrySeparator);			
			}
		}

		for (indicatorsHeaderEnum h : indicatorsHeaderEnum.values())
			w.write(h.toString() + entrySeparator);

		w.write("\n");
	}
	
	private String cleanString(String s) {
		s = StringUtils.trimToEmpty(s); // also takes care of ( null -> "" )
		s = s.replaceAll(";", ",");
		s = s.replaceAll("\\R+", "");
		s.trim();
		return s;
	}
	
	private <T> String newCellString(T p, Function<T, String> f) {
		String s = "";
		try {
			s += cleanString(f.apply(p));
		} catch (Exception e) {
			// Cell remains empty
		}
		s += entrySeparator;
		return s;
	}

	public void formatCSV(List<Process> processes, Map<String, ProductFlow> flowProperties, Writer w)
			throws IOException {
		
		CategoryTranslator ct = new CategoryTranslator(DataSetType.PROCESS, ConfigurationService.INSTANCE.getDefaultClassificationSystem());

		for (Process process : processes) {

			StringBuilder sb = new StringBuilder();
			sb.append(process.getUuidAsString());
			sb.append(entrySeparator);
			sb.append(process.getDataSetVersion());
			sb.append(entrySeparator);
			
			// Append names for all preferred languages
			ConfigurationService.INSTANCE.getPreferredLanguages().stream()
					.map(lang -> newCellString(process, p -> p.getBaseName().getValue(lang)))
					.forEach(nameCellString -> sb.append(nameCellString));
				
			sb.append(newCellString(process, p -> p.getClassification().getIClassList().stream().map(IClass::getName)
					.collect(Collectors.joining("\' / \'", "\'", "\'"))));

			ConfigurationService.INSTANCE.getTranslateClassificationCSVlanguages().stream()
					.map(lang -> newCellString(process,
							p -> p.getClassification().getIClassList().stream()
									.map(c -> ct.translateTo(c.getClId(), lang))
									.filter(c -> c != null && !c.trim().isEmpty())
									.map(c -> "'" + c + "'")
									.collect(Collectors.joining(" / "))))
					.forEach(cellString -> sb.append(cellString));

			sb.append(newCellString(process, p -> p.getComplianceSystems().stream().map(IComplianceSystem::getName)
					.collect(Collectors.joining("\' / \'", "\'", "\'"))));
			
			// Location
			sb.append(newCellString(process, p -> p.getLocation()));
			
			// Sub-type
			sb.append(newCellString(process, p -> p.getSubType().getValue()));
			
			// Reference year
			sb.append(newCellString(process, p -> p.getTimeInformation().getReferenceYear().toString()));
			
			// Valid until
			sb.append(newCellString(process, p -> p.getTimeInformation().getValidUntil().toString()));
			
			// URL
			sb.append(newCellString(process, p -> ConfigurationService.INSTANCE.getNodeInfo().getBaseURL()
					+ "processes/" + p.getUuidAsString() + "?version=" + p.getVersion().getVersionString()));

			// Declaration Owner
			sb.append(newCellString(process, p -> p.getOwnerReference().getShortDescription().getDefaultValue()));
			
			// Publication date
			SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
			sb.append(newCellString(process, p -> dateFormatter.format(p.getPublicationDateOfEPD())));
			
			// Registration number
			sb.append(newCellString(process, p -> p.getRegistrationNumber()));
			
			// Registration authority
			sb.append(newCellString(process, p -> p.getReferenceToRegistrationAuthority().getShortDescription().getDefaultValue()));

			// Add predecessor data
			try {
				Comparator<? super GlobalReference> comparator = new DataSetUUIDVersionComparator();
				GlobalReference predecessorReference = process.getPrecedingDataSetVersions().stream()
						.sorted(comparator)
						.collect(Collectors.toList())
						.get(0); // Throws IndexOutOfBoundsException if no predecessor found.
	
				ProcessDao pDao = new ProcessDao();
				Process predecessor = pDao.getByUuid(predecessorReference.getRefObjectId());
				
				// Predecessor UUID,
				// using the reference in case the predecessor hasn't been found.
				sb.append(newCellString(predecessorReference, ref -> ref.getRefObjectId()));
				
				// Predecessor Version
				sb.append(newCellString(predecessorReference, ref -> ref.getVersionAsString()));
				
				// Predecessor URL
				sb.append(newCellString(predecessorReference, ref -> ref.getHref()));
				
			} catch (IndexOutOfBoundsException iobe ) {
				// No Predecessor, so we simply skip the corresponding columns
				sb.append(entrySeparator + entrySeparator + entrySeparator);
			}
			
			sb.append(newCellString(process, p -> format((double) p.getReferenceExchanges().get(0).getMeanAmount())));
			
			sb.append(newCellString(process, p -> p.getReferenceExchanges().get(0).getReferenceUnit()));

			ProductFlow productFlow = flowProperties.get(process.getUuid().getUuid());
			if(productFlow != null) {
				// Product flow UUID
				sb.append(newCellString(productFlow, pf -> pf.getUuidAsString()));
				
				// Product flow name
				sb.append(newCellString(productFlow, pf -> pf.getName().getDefaultValue()));
				
				Map<String, Double> matPropMap = new HashMap<String, Double>();
				for (MaterialProperty matProp : productFlow.getMaterialProperties()) {
					matPropMap.put(matProp.getDefinition().getName(), matProp.getValue());
				}
				
				if (matPropMap.containsKey("bulk density")) {
					sb.append(format(matPropMap.get("bulk density")));
				}
				sb.append(entrySeparator);
				
				if (matPropMap.containsKey("grammage")) {
					sb.append(format(matPropMap.get("grammage")));
				}
				sb.append(entrySeparator);
				
				if (matPropMap.containsKey("gross density")) {
					sb.append(format(matPropMap.get("gross density")));
				}
				sb.append(entrySeparator);
				
				if (matPropMap.containsKey("layer thickness")) {
					sb.append(format(matPropMap.get("layer thickness")));
				}
				sb.append(entrySeparator);
				
				if (matPropMap.containsKey("productiveness")) {
					sb.append(format(matPropMap.get("productiveness")));
				}
				sb.append(entrySeparator);
				
				if (matPropMap.containsKey("linear density")) {
					sb.append(format(matPropMap.get("linear density")));
				}
				sb.append(entrySeparator);
				
				if (matPropMap.containsKey("conversion factor to 1 kg")) {
					sb.append(format(matPropMap.get("conversion factor to 1 kg")));
				}
				sb.append(entrySeparator);
			} else {
				// we skipped 9 fields
				for (int i=0; i<9; i++)
					sb.append(entrySeparator);
			}


			String metaData = sb.toString();

			for (String module : process.getDeclaredModules()) {
				w.write(metaData);

				w.write(module);
				w.write(entrySeparator);

				for (indicatorsHeaderEnum h : indicatorsHeaderEnum.values())
					w.write(h.getResultValue(process, module) + entrySeparator);

				w.write("\n");
			}
		}
	}

	private static String format(Double d) {
		DecimalFormatSymbols decimalSymbols = DecimalFormatSymbols.getInstance();
		decimalSymbols.setDecimalSeparator(decimalSeparator.separator);
		return new DecimalFormat("#.####################", decimalSymbols).format(d);
	}

	public enum DecimalSeparator {

		DOT("dot", '.'), COMMA("comma", ',');

		private String value;
		public char separator;

		DecimalSeparator(String value, char separator) {
			this.value = value;
			this.separator = separator;
		}

		public static ExportType fromValue(String value) {
			for (ExportType enumValue : ExportType.values()) {
				if (enumValue.getValue().equals(value))
					return enumValue;
			}
			return null;
		}

		public String getValue() {
			return value;
		}
	}
}
