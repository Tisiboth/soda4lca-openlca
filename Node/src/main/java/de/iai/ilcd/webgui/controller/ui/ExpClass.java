package de.iai.ilcd.webgui.controller.ui;

import java.util.Set;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.iai.ilcd.model.common.DataSet;
import de.iai.ilcd.model.dao.DependenciesMode;
import de.iai.ilcd.model.dao.LifeCycleModelDao;
import de.iai.ilcd.model.lifecyclemodel.LifeCycleModel;
import de.iai.ilcd.persistence.PersistenceUtil;

@Named
@Deprecated
public class ExpClass {

	private static Logger logger = LoggerFactory.getLogger(ExpClass.class);

	public String erd = "123";

	public void doStuff() {
		erd = "789";
		logger.warn("Button has been clicked");

		LifeCycleModel lcm = new LifeCycleModel();
		LifeCycleModelDao dao = new LifeCycleModelDao();
		
		lcm = dao.getByUuid("20062015-184a-41b8-8fa6-49e999cbd101");
		
		
		Set<DataSet> deps = dao.getDependencies(lcm, DependenciesMode.ALL);
		
		System.out.println(deps);
		
		
//		
//		
//		lcm.setUuid(new Uuid("ec389206-53dd-48bc-8f21-4e0669077300"));
//		try {
//			lcm.setVersion(new DataSetVersion(1, 3, 4));
//		} catch (FormatException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		lcm.setImportDate(new Date(System.currentTimeMillis()));\\

//		save(lcm);
	}

	public LifeCycleModel find(String UUID, int Major) {
		EntityManager em = PersistenceUtil.getEntityManager();
		Query q = em.createQuery(
				"SELECT lcm FROM LifeCycleModel lcm WHERE lcm.uuid.uuid = :uuid AND lcm.version.majorVersion = :ver",
				LifeCycleModel.class);
		q.setParameter("uuid", UUID);
		q.setParameter("ver", Major);
		q.setMaxResults(1);
		return (LifeCycleModel) q.getSingleResult();
	}

	public void save(Object obj) {
		EntityManager em = PersistenceUtil.getEntityManager();
		em.getTransaction().begin();
		em.persist(obj);
		em.getTransaction().commit();
		em.close(); // dont close later
	}

	public String getErd() {
		return erd;
	}

	public void setErd(String erd) {
		this.erd = erd;
	}
}