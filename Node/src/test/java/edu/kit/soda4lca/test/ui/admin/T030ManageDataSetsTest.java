package edu.kit.soda4lca.test.ui.admin;

import com.codeborne.selenide.testng.ScreenShooter;
import edu.kit.soda4lca.test.ui.AbstractUITest;
import edu.kit.soda4lca.test.ui.main.TestContext;
import edu.kit.soda4lca.test.ui.main.TestFunctions;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;

/**
 * Delete a dataset from every type and test if it gets really deleted
 * 
 * @author mark.szabo
 * 
 */
@Listeners({ScreenShooter.class})
public class T030ManageDataSetsTest extends AbstractUITest {

	// initializing the log
	protected final static Logger log = org.apache.log4j.Logger.getLogger( T030ManageDataSetsTest.class );

	@Override
	protected List<List<String>> getDBDataSetFileName() { return Arrays.asList(Arrays.asList("DB_post_T015ImportExportTest.xml")); };

	@Test( priority = 301 )
	public void manageProcesses() throws InterruptedException {
		manageSomething( "process", "process", false );
	}

	@Test( priority = 302 ) 
	public void manageLCIAMethods() throws InterruptedException {
		manageSomething( "lciaMethod", "lciamethod", false );
	}

	@Test( priority = 303 )
	public void manageElementaryFlows() throws InterruptedException {
		manageSomething( "elementaryFlow", "flow", false );
	}

	@Test( priority = 304 ) 
	public void manageProductFlows() throws InterruptedException {
		manageSomething( "productFlow", "flow", false );
	}

	@Test( priority = 305 ) 
	public void manageFlowProperties() throws InterruptedException {
		manageSomething( "flowProperty", "flowproperty", false );
	}

	@Test( priority = 306 )
	public void manageUnitGroups() throws InterruptedException {
		manageSomething( "unitGroup", "unitgroup", true );
	}

	@Test( priority = 307 )
	public void manageSources() throws InterruptedException {
		manageSomething( "source", "source", false );
	}

	@Test( priority = 308 )
	public void manageContacts() throws InterruptedException {
		manageSomething( "contact", "contact", false );
	}

	/**
		 * Delete a dataset from every type and test if it gets really deleted. It's almost the same for every type, so this
		 * method can delete from which you want
		 * 
		 * @param menu
		 *            Language property "admin." + menu + ".manageList" contains the menu link text
		 * @param link
		 *            Table id: "[@id='" + link+ "Table_data']"
		 * @param isitUnitGroup
		 *            usually almost any dataset can be deleted. But because of dependences by unit Group we have to delete
		 *            a specified one
		 * @throws InterruptedException
		 */
		public void manageSomething( String menu, String link, boolean isitUnitGroup ) throws InterruptedException {
			TestContext.getInstance().getDriver().manage().deleteAllCookies();
			log.info( TestContext.lang.getProperty( "admin." + menu + ".manageList" ) + " test started" );
			// login as admin
			TestFunctions.login( "admin", "default", true, true );
			// click on Admin area
			TestFunctions.gotoAdminArea();
			
			// Create an action for mouse-moves
			Actions action = new Actions( TestContext.getInstance().getDriver() );
			// Mouse over the menu 'User'
			action.moveToElement( TestContext.getInstance().getDriver().findElement( By.linkText( TestContext.lang.getProperty( "admin.dataset.manageList" ) ) ) ).build()
					.perform();
			// Mouse over and click the submenu 'Manage ***'
			action.moveToElement( TestContext.getInstance().getDriver().findElement( By.linkText( TestContext.lang.getProperty( "admin." + menu + ".manageList" ) ) ) ).click()
					.build().perform();
			log.trace( "Change datastock" );

			Thread.sleep( TestContext.wait * 2 );

			// Choose data stock RootStock1
//			TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='admin_header']/div[2]//table/tbody/tr[1]/td[2]/div" ) ).click();
			TestFunctions.findAndWaitOnElement( By.xpath( "//*[contains(@class, 'selectDataStockHTMLClass')]") ).click();
			TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@data-label='RootStock1']" ) ).click();
			
			// wait
			Thread.sleep( TestContext.wait * 2 );
	
			// TestFunctions.findandwaitanElement(By.xpath(".//*[ends-with(@id, 'selectDataStock_panel')]/div/ul/li[2]")).click();
			(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.visibilityOfElementLocated( By.xpath( ".//*[@id='" + link
	 + "Table_data']/tr[1]/td[1]/div/div[2]/span" ) ) );
			/*
			 * UnitGroups has a lot of dependences
			 * so we have to delete a specific one, which can be deleted
			 * it will be "Units of volume*length"
			 * in other cases just delete the first one
			 */
			String uuid;
			log.trace( "select dataset" );
	
			// read the UUID of the second element
			uuid = TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='" + link + "Table:1:nameColumnContentUuid']" ) ).getText();
			// select the first item
			TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='" + link + "Table_data']/tr[1]/td[1]/div/div[2]/span" ) ).click();
	
			// wait for Delete button to enabled
			(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.elementToBeClickable( By.xpath( ".//*[@id='" + link
					+ "Tablebtn']" ) ) );
			// click Delete Selected Entries
			log.trace( "Click delete" );
			TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='" + link + "Tablebtn']" ) ).click();
			
			// wait
			Thread.sleep( TestContext.wait * 2 );

			// Sure? Click OK			
			WebElement btnOk = TestFunctions.findDialogButton(TestContext.lang.getProperty( "admin.confirmDlg.delete" ), TestContext.lang.getProperty( "admin.ok" ));
			btnOk.click();
			
			Thread.sleep(TestContext.wait);

			// wait
			Thread.sleep( TestContext.wait * 2 );

			// check the message:
			String overviewMessagePattern = TestContext.lang.getProperty("facesMsg.deleteOverview");
			String overviewMessage = MessageFormat.format(overviewMessagePattern, 1,0,0);
			boolean entryDeleted = TestFunctions.findAndWaitOnElement(By.xpath(".//div[contains(@id,'messages')]"))
					.getText().contains(overviewMessage);
			
			if (!entryDeleted) {
				org.testng.Assert
						.fail("Wrong message: "
								+ TestFunctions.findAndWaitOnElement(By.xpath(".//div[contains(@id,'messages')]"))
										.getText()
								+ ". It should have contained: " + overviewMessage);
			}
			
			// wait
			Thread.sleep( TestContext.wait * 2 );
			// refresh page
			//		TestContext.getInstance().getDriver().navigate().refresh();
			
			log.trace( "check if element was deleted correctly" );
			// check if element was deleted correctly
			if ( TestFunctions.isElementNotPresent( By.linkText( uuid ) ) )
				org.testng.Assert.fail( "An element should have been deleted, but it is still there" );
	
			log.info( TestContext.lang.getProperty( "admin." + menu + ".manageList" ) + " test finished" );
		}
}
