package edu.kit.soda4lca.test.ui.admin;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.codeborne.selenide.testng.ScreenShooter;

import edu.kit.soda4lca.test.ui.AbstractUITest;
import edu.kit.soda4lca.test.ui.main.TestContext;
import edu.kit.soda4lca.test.ui.main.TestFunctions;

@Listeners({ScreenShooter.class})
public class T041SearchProcessesTest extends AbstractUITest {

	@Override
	protected List<List<String>> getDBDataSetFileName() { return Arrays.asList(Arrays.asList("DB_post_T015ImportExportTest.xml")); };

	protected final static Logger log = org.apache.log4j.Logger.getLogger( T041SearchProcessesTest.class );

	@Test( priority = 411 )
	public void searchProcesses() throws InterruptedException {
		log.info( "'Search processes' test started" );
		TestContext.getInstance().getDriver().manage().deleteAllCookies();
	
		// login as admin
		TestFunctions.login( "admin", "default", true, true );
		Thread.sleep( 3 * TestContext.wait );
	
		TestContext.getInstance().getDriver().get(TestContext.PRIMARY_SITE_URL + "?stock=RootStock1");
	
		searchProcess( "test", 2, "1999", "2002", "LCI result", false );
		searchProcess( "", -1, "", "", "LCI result", true );
	
		log.info( "'Search processes' test finished" );
	}

	// @Test( priority = 412, dependsOnMethods = { "searchProcesses" } )
	public void searchProcessesAcrossNetwork() throws InterruptedException {
		log.info( "'Search processes across network' test started" );
		TestContext.getInstance().getDriver().manage().deleteAllCookies();
	
		// login as admin
		TestFunctions.login( "admin", "default", true, true );
		Thread.sleep( 3 * TestContext.wait );
	
		// searchProcessAcrossNetwork( "", "", -1, "", "", "", false, true );
		// searchProcessAcrossNetwork( "registry1", "test", 2, "1999", "2005", "LCI result", false, false );
		// searchProcessAcrossNetwork( "registry1", "", -1, "", "", "", true, false );
	
		log.info( "'Search processes across network' test finished" );
	}

	public void searchProcess( String name, int location, String yearLower, String yearUpper, String type, boolean correct ) throws InterruptedException {
		log.trace( "Searching process " );

		Actions action = new Actions( TestContext.getInstance().getDriver() );
	
		String searchLabel = TestContext.lang.getProperty( "public.search" ) + " " + TestContext.lang.getProperty( "common.processes" );
		action.moveToElement( TestContext.getInstance().getDriver().findElement( By.linkText( searchLabel ) ) ).build()
				.perform();
		TestFunctions.findAndWaitOnElement( By.xpath( "//a[contains(.,'" + searchLabel + "')]" ) ).click();
		Thread.sleep( TestContext.wait );

		// Fill in the form
		if ( name.length() > 0 ) {
			TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='name']" ) ).clear();
			TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='name']" ) ).sendKeys( name );
		}
		if ( location != -1 ) {
			TestFunctions.findAndWaitOnElement( By.xpath( "//td//div[@id='location']//ul/li[contains(.,'" + location + "')]" ) ).click();
		}
		if ( yearLower.length() > 0 ) {
			TestFunctions.findAndWaitOnElement( By.xpath( "//label[@id='referenceYearLower_label']" ) ).click();
			TestFunctions.findAndWaitOnElement( By.xpath( ".//div[@id='referenceYearLower_panel']/div/ul/li[contains(.,'" + yearLower + "')]" ) ).click();
			Thread.sleep( TestContext.wait );
		}
		if ( yearUpper.length() > 0 ) {
			TestFunctions.findAndWaitOnElement( By.xpath( "//label[@id='referenceYearUpper_label']" ) ).click();
			TestFunctions.findAndWaitOnElement( By.xpath( ".//div[@id='referenceYearUpper_panel']/div/ul/li[contains(.,'" + yearUpper + "')]" ) ).click();
			Thread.sleep( TestContext.wait );
		}
		if ( type.length() > 0 ) {
			TestFunctions.findAndWaitOnElement( By.xpath( "//label[@id='type_label']" ) ).click();
			TestFunctions.findAndWaitOnElement( By.xpath( ".//div[@id='type_panel']/div/ul/li[contains(.,'" + type + "')]" ) ).click();
			Thread.sleep( TestContext.wait );
		}
		Thread.sleep( 3 * TestContext.wait );

		// Click Save
		TestFunctions.findAndWaitOnElement( By.xpath( "//button[contains(.,'" + TestContext.lang.getProperty( "public.search" ) + "')]" ) ).click();
		Thread.sleep( 3 * TestContext.wait );
		// check the result

		if ( correct ) {
			(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.visibilityOfElementLocated( By
					.xpath( ".//*[@id='processTable_data']" ) ) );
			if ( !TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='processTable_data']//td[1]" ) ).isDisplayed() )
				org.testng.Assert.fail( "Error." );
			else {
				log.trace( "Process searched succesfull" );
			}
		}
		else {
			if ( !TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='processTable_data']//td" ) ).getText().contains(
					TestContext.lang.getProperty( "common.list.noneFound" ).substring( 0, 10 ) ) )
				org.testng.Assert.fail( "Error." );
		}
	}

	public void searchProcessAcrossNetwork( String registry, String name, int location, String yearLower, String yearUpper, String type, boolean correct,
			boolean lackOfReg )
			throws InterruptedException {
		log.trace( "Searching process across network " );

		Actions action = new Actions( TestContext.getInstance().getDriver() );

		String searchLabel = TestContext.lang.getProperty( "public.search" ) + " " + TestContext.lang.getProperty( "common.processes" );
		action.moveToElement( TestContext.getInstance().getDriver().findElement( By.linkText( searchLabel ) ) ).build().perform();
		TestFunctions.findAndWaitOnElement( By.xpath( "//a[contains(.,'" + searchLabel + "')]" ) ).click();
		Thread.sleep( TestContext.wait );

		// Fill in the form
		if ( name.length() > 0 ) {
			TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='name']" ) ).clear();
			TestFunctions.findAndWaitOnElement( By.xpath( ".//input[@id='name']" ) ).sendKeys( name );
		}
		if ( location != -1 ) {
			TestFunctions.findAndWaitOnElement( By.xpath( "//td/div/ul/li[contains(.,'" + location + "')]" ) ).click();
		}
		if ( yearLower.length() > 0 ) {
			TestFunctions.findAndWaitOnElement( By.xpath( "//label[@id='referenceYearLower_label']" ) ).click();
			TestFunctions.findAndWaitOnElement( By.xpath( ".//div[@id='referenceYearLower_panel']/div/ul/li[contains(.,'" + yearLower + "')]" ) ).click();
			Thread.sleep( TestContext.wait );
		}
		if ( yearUpper.length() > 0 ) {
			TestFunctions.findAndWaitOnElement( By.xpath( "//label[@id='referenceYearUpper_label']" ) ).click();
			TestFunctions.findAndWaitOnElement( By.xpath( ".//div[@id='referenceYearUpper_panel']/div/ul/li[contains(.,'" + yearUpper + "')]" ) ).click();
			Thread.sleep( TestContext.wait );
		}
		if ( type.length() > 0 ) {
			TestFunctions.findAndWaitOnElement( By.xpath( "//label[@id='type_label']" ) ).click();
			TestFunctions.findAndWaitOnElement( By.xpath( ".//div[@id='type_panel']/div/ul/li[contains(.,'" + type + "')]" ) ).click();
			Thread.sleep( TestContext.wait );
		}
		Thread.sleep( 3 * TestContext.wait );

		TestFunctions.findAndWaitOnElement( By.xpath( "//input[@id='distributed']" ) ).click();
		Thread.sleep( TestContext.wait );
		if ( registry.length() > 0 ) {
			new Select( TestFunctions.findAndWaitOnElement( By.xpath( "//select[@id='reg']" ) ) ).selectByVisibleText( registry );
		}
		// Click Save
		TestFunctions.findAndWaitOnElement( By.xpath( "//button[contains(.,'" + TestContext.lang.getProperty( "public.search" ) + "')]" ) ).click();
		// wait a little
		Thread.sleep( 3 * TestContext.wait );

		// check the resut

		if ( correct ) {
			(new WebDriverWait( TestContext.getInstance().getDriver(), TestContext.timeout )).until( ExpectedConditions.visibilityOfElementLocated( By
					.xpath( ".//*[@id='processTable_data']" ) ) );
			if ( !TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='processTable_data']//td[1]" ) ).isDisplayed() )
				org.testng.Assert.fail( "Error." );
			else {
				log.trace( "Process searched succesfull" );
			}
		}
		else if ( lackOfReg ) {
			if ( !TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='messages']" ) ).getText().contains( "reg: Validation Error: Value is required." ) )
				org.testng.Assert.fail( "Error." );
		}
		else {
			if ( !TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='processTable_data']//td" ) ).getText().contains(
							TestContext.lang.getProperty( "common.list.noneFound" ).substring( 0, 10 ) ) )
				org.testng.Assert.fail( "Error." );
		}
	}

}
