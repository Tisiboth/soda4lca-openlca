package edu.kit.soda4lca.test.ui.basic;

import java.util.Properties;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import edu.kit.soda4lca.test.ui.main.TestContext;
import edu.kit.soda4lca.test.ui.main.TestFunctions;


/**
 * Try to log in with different user/password combinations with rememberme flag set or not set
 * 
 * @author sarai.eilebrecht
 * 
 */
public class T002RememberMeTest {

	// initializing the log
	protected final static Logger log = org.apache.log4j.Logger.getLogger( T001Absolut_BasicTest.class );

	

	/**
	 * try to log in with different user/password combinations with rememberme flag set or not set and 
	 * try to update page and see link to admin area after session expires
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public static void rememberMe() throws InterruptedException {
		// Login test with rememberme
		// Session needs to expire to test whether rememberme is correctly set. 
		// Hence thread must wait at least as long as timeout. 
		Properties lang = TestContext.lang;
		TestContext testContext = TestContext.getInstance();
		WebDriver ff = testContext.getDriver();
		
		log.info( "Login test started" );
		TestContext.getInstance().getDriver().manage().deleteAllCookies();

		log.debug( "Basic Remember Me test started - Try to log in with different credentials" );
		
		TestFunctions.login("admin", "default", true, true, 1, true);
		Thread.sleep(90000);
		ff.navigate().refresh();
		log.debug( "Page is refreshed after first login with rememberme flag set" );
		if ( !TestFunctions.isElementNotPresent( By.linkText( lang.getProperty( "admin.adminArea" ) ) ) )
			org.testng.Assert.fail( "Admin area button is not presented but rememberme is set." );

		ff.manage().deleteAllCookies();
		ff.navigate().refresh();
		log.debug( "Page is refreshed after all cookies are deleted" );
		if ( TestFunctions.isElementNotPresent( By.linkText( lang.getProperty( "admin.adminArea" ) ) ) )
			org.testng.Assert.fail( "Admin area button is presented after rememberme cookie is deleted." );
		TestFunctions.login("admin", "default", true, true, 1, false);
		Thread.sleep(90000);
		ff.navigate().refresh();
		log.debug( "Page is refreshed after login without rememberme flag set" );
		if ( TestFunctions.isElementNotPresent( By.linkText( lang.getProperty( "admin.adminArea" ) ) ) )
			org.testng.Assert.fail( "Admin area button is presented after session is expired." );
		TestFunctions.login( "admin", "admin", false, true, 1, true );
		log.debug("Try to log in with incorrect password");
		if ( TestFunctions.isElementNotPresent( By.linkText( lang.getProperty( "admin.adminArea" ) ) ) )
			org.testng.Assert.fail( "Admin area button is presented but password is not correct." );
		log.info( "Basic Remember Me test finished" );
	}

}
