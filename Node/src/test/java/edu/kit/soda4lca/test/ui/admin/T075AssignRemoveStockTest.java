package edu.kit.soda4lca.test.ui.admin;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.springframework.util.StringUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.codeborne.selenide.testng.ScreenShooter;
import com.codeborne.selenide.WebDriverRunner;

import static com.codeborne.selenide.Selenide.*;
import de.iai.ilcd.service.job.DataSetsTypes;
import edu.kit.soda4lca.test.ui.AbstractUITest;
import edu.kit.soda4lca.test.ui.main.TestContext;
import edu.kit.soda4lca.test.ui.main.TestFunctions;

@Listeners({ScreenShooter.class})
public class T075AssignRemoveStockTest extends AbstractUITest {
	
	   private int assignButton = 8;
	
	    // initializing the log
		protected final static Logger log = org.apache.log4j.Logger.getLogger(T075AssignRemoveStockTest.class);
	
		@Override
		protected List<List<String>> getDBDataSetFileName() {
			return Arrays.asList(Arrays.asList("DB_post_T015ImportExportTest.xml"));
		}
		
		/**
		 * Logs in.
		 * @throws InterruptedException
		 */
		@BeforeClass
		public void login() throws InterruptedException {
			// import some data
			WebDriverRunner.clearBrowserCache();
			log.debug("Trying to login");
			// login as admin
			TestFunctions.login("admin", "default", true, true);

			// click on Admin area
			TestFunctions.gotoAdminArea();
			// wait for the site to load
			TestFunctions.waitUntilSiteLoaded();
		}

	/**
	 * Tests assign/remove functionality and displayed job queue.
	 * @throws InterruptedException
	 */
	@Test(priority = 301)
	public void testAssignRemoveData() throws InterruptedException {
		dataStock_assign("Process", "processes");
		dataStock_remove("Process", "processes");
	}
	
	/**
	 * Tries to assign data stock. Checks after assigning by calling if all data stocks were assigned as expected and if no errors 
	 * occurred during assigning.
	 * @param action The action for selecting menu pages
	 * @param alreadyProcessed True if given dataSets was already processed
	 * @throws InterruptedException
	 */
	public void dataStock_assign(String dataSetType, String dataSetType2) throws InterruptedException {
		Properties lang = TestContext.lang;
		String stockName = "SimpleStock1";
		log.debug("Going to 'Manage List ' page.");
		
		$(By.linkText(TestContext.lang.getProperty("common.stock"))).hover();
		// Click Manage Data Stocks
		$(By.linkText(TestContext.lang.getProperty("admin.stock.manageList"))).click();
		TestFunctions.waitUntilSiteLoaded();

		// click assign data stock button in SimpleStock1 data stock
		$x(".//*[text()='SimpleStock1']/../../td[" + assignButton + "]/button[*]").click();

		if (!"Process".equals(dataSetType)) {
			// switch to another data set
			TestFunctions.findAndWaitOnElement(By.linkText(lang.getProperty("common." + dataSetType2))).click();
		}
		// Test if data set is empty
		TestFunctions.waitUntilSiteLoaded();
		//$x(".//*[@id='stockTabs:dataSetTabView:ct" + dataSetType + "DataTable_data']/tr[1]/td[2]").waitUntil(exist, TestContext.wait * 8);
		if (TestFunctions.isElementNotPresent(
				By.xpath(".//*[@id='stockTabs:dataSetTabView:ct" + dataSetType + "DataTable_data']/tr[1]/td[3]")))
			org.testng.Assert.fail("logical data stock contains at least one element.");

		TestFunctions.findAndWaitOnElement(By.linkText(lang.getProperty("admin.tab.mainInfo"))).click();

		TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='stockTabs:org_label']" ) ).click();
		Thread.sleep( TestContext.wait );

		TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='stockTabs:org_panel']/div/ul/li[" + 2 + "]" ) ).click();

		TestFunctions.findAndWaitOnElement(By.linkText(lang.getProperty("admin.stock.batchAssignRemove"))).click();
		
		TestFunctions.findAndWaitOnElement(
				By.xpath(".//*[@id='stockTabs:selectionRadioBtnTab']/tbody[1]/tr[1]/td[1]/div[1]")).click();

		// test whether data stocks list is not empty and whether
		// desired data set is occurs in assignable data set list
		TestFunctions
		.findAndWaitOnElement(By.xpath( ".//*[text()='RootStock1']/../../td[1]/div[*]"))
		.click();
		
		// Checking if all datasets types exist as a checkboxes
		for (DataSetsTypes dataSetsType : DataSetsTypes.values()) {
			String label =TestContext.lang.getProperty(dataSetsType.getDisplayName()  ) ;
			TestFunctions.findAndWaitOnElement( By.xpath( "//td[text()='" + label + "']" ) );
		}

		// click on assign button to assign a data set
		TestFunctions.findAndWaitOnElement(By.id("stockTabs:assignAllBtn")).click();

		if ("Process".equals(dataSetType)) 
			TestFunctions.selectOptionInDataSetDialogue(4);
		else
			TestFunctions.selectOptionInDataSetDialogue(1);

		TestFunctions.goToPageByAdminMenu("admin.jobs", "admin.jobs.showList");
		log.debug("Going to 'Show jobs' page.");
		TestFunctions.waitUntilSiteLoaded();
		String state = TestFunctions.getFirstTableEntryAtColumn("showJobsTable", new Long(11));
		
		log.debug("Checking new job entry.");
		TestFunctions.waitUntilSiteLoaded();
		if (!TestFunctions.isLastTableEntryAtColumnEqual("showJobsTable", new Long(2), "admin")) {
			org.testng.Assert.fail("Incorrect user name is shown.");
		}
		if (!TestFunctions.isLastTableEntryAtColumnEqual("showJobsTable", new Long(3), "assign")) {
			org.testng.Assert.fail("Incorrect job type is shown.");
		}
		
		if (log.isDebugEnabled())
			log.debug("state is: '" + state + "'");
		while (state.equals("RUNNING")) {
			TestFunctions.clickButtonWithLabel("refresh");
			TestFunctions.waitUntilSiteLoaded();
			state = TestFunctions.getFirstTableEntryAtColumn("showJobsTable", new Long(11));
			log.debug("updating job page");
		}
		TestFunctions.waitUntilSiteLoaded();
		log.debug("job is complete.");
		
		String description = "assign 53 datasets to " + stockName;
		
		if (!TestFunctions.isLastTableEntryAtColumnEqual("showJobsTable", new Long(4), description)) {
			org.testng.Assert.fail("Incorrect description is shown.");
		}
		Long errorCount = Long.valueOf( TestFunctions.getFirstTableEntryAtColumn("showJobsTable", new Long(8))).longValue();
		Long infoCount = Long.valueOf(TestFunctions.getFirstTableEntryAtColumn("showJobsTable", new Long(9))).longValue();
		Long successCount = Long.valueOf(TestFunctions.getFirstTableEntryAtColumn("showJobsTable", new Long(10))).longValue();
		
		TestFunctions.findAndWaitOnElement(By.xpath(".//*[@id='showJobsTable_data']/tr[1]/td[1]/div")).click();
		String jobLog = TestFunctions.getSecondTableEntryAtColumn("showJobsTable", new Long(1));

		log.debug("Checking amount of data sets that were assigned correctly.");
		if (errorCount != 0 || infoCount != 1 || successCount != 52 || !(state.equals("COMPLETE"))) {
			log.trace("Error is: " + errorCount);
			log.trace("infosCount is: " + infoCount);
			log.trace("state is: " + state);
			org.testng.Assert.fail("Job did not complete correctly!");
		}
		log.debug("Checking job log.");
			
		Long assignedDataSets = new Long(StringUtils.countOccurrencesOf(jobLog, "SUCCESS"));

		if (log.isTraceEnabled()) {
			log.trace("jobLog is: " + jobLog);
			log.trace("number of assigned data sets is: " + assignedDataSets);
		}

		if (assignedDataSets != 53) {
			org.testng.Assert.fail("Incorrect ammount of files was correctly assigned!");
		}
		
		int infoDataSets = StringUtils.countOccurrencesOf(jobLog, "INFO");
		if (infoDataSets != 2) {
			if (log.isDebugEnabled()) {
				log.debug("number of failed assignes: " + StringUtils.countOccurrencesOf(jobLog, "ERROR"));
				log.debug("number of successful assignes: " + StringUtils.countOccurrencesOf(jobLog, "SUCCESS"));
				log.debug("Number of already pushed assignes: " + StringUtils.countOccurrencesOf(jobLog, "INFO"));
			}
			org.testng.Assert.fail("A data set was unexpectedly not assigned!");
		}
			
		
		
		TestFunctions.goToPageByAdminMenu("admin.dataset.manageList", "admin.process.manageList");
		TestFunctions.waitUntilSiteLoaded();
		TestFunctions.selectDataStock(stockName);
		TestFunctions.targetTableContainsNumberDataSets("processTable", new Long(5));
		
		TestFunctions.goToPageByAdminMenu("admin.dataset.manageList", "admin.lciaMethod.manageList");
		TestFunctions.waitUntilSiteLoaded();
		TestFunctions.targetTableContainsNumberDataSets("lciamethodTable", new Long(5));
		
		TestFunctions.goToPageByAdminMenu("admin.dataset.manageList", "admin.elementaryFlow.manageList");
		TestFunctions.waitUntilSiteLoaded();
		TestFunctions.targetTableContainsNumberDataSets("flowTable", new Long(7));
		
		TestFunctions.goToPageByAdminMenu("admin.dataset.manageList", "admin.productFlow.manageList");
		TestFunctions.waitUntilSiteLoaded();
		TestFunctions.targetTableContainsNumberDataSets("flowTable", new Long(5));
		
		TestFunctions.goToPageByAdminMenu("admin.dataset.manageList", "admin.flowProperty.manageList");
		TestFunctions.waitUntilSiteLoaded();
		TestFunctions.targetTableContainsNumberDataSets("flowpropertyTable", new Long(5));

		TestFunctions.goToPageByAdminMenu("admin.dataset.manageList", "admin.unitGroup.manageList");
		TestFunctions.waitUntilSiteLoaded();
		TestFunctions.targetTableContainsNumberDataSets("unitgroupTable", new Long(16));

		TestFunctions.goToPageByAdminMenu("admin.dataset.manageList", "admin.source.manageList");
		TestFunctions.waitUntilSiteLoaded();
		TestFunctions.targetTableContainsNumberDataSets("sourceTable", new Long(5));

		TestFunctions.goToPageByAdminMenu("admin.dataset.manageList", "admin.contact.manageList");
		TestFunctions.waitUntilSiteLoaded();
		TestFunctions.targetTableContainsNumberDataSets("contactTable", new Long(5));
		
		login();
	}

	/**
	 * Tries to remove data stock. Checks after removing by calling if all data stocks were removed as expected and if no errors 
	 * occurred during removing.
	 * @param action The action for selecting menu pages
	 * @param alreadyProcessed True if given push config entry was already pushed
	 * @throws InterruptedException
	 */
	public void dataStock_remove(String dataSetType, String dataSetType2) throws InterruptedException {
		Properties lang = TestContext.lang;
		String stockName = "SimpleStock1";
		log.debug("Going to 'Manage list' page.");
		$(By.linkText(TestContext.lang.getProperty("common.stock"))).hover();
		// Click Manage Data Stocks
		$(By.linkText(TestContext.lang.getProperty("admin.stock.manageList"))).click();
		// click assign data stock button in SimpleStock1 data stock
		TestFunctions
				.findAndWaitOnElement(By.xpath(".//*[text()='SimpleStock1']/../../td[" + assignButton + "]/button[*]"))
				.click();

		if (!"Process".equals(dataSetType)) {
			// switch to another data set
			TestFunctions.findAndWaitOnElement(By.linkText(lang.getProperty("common." + dataSetType2))).click();
		}
		// Test if data set is empty
		TestFunctions.waitUntilSiteLoaded();

		TestFunctions.findAndWaitOnElement(By.linkText(lang.getProperty("admin.tab.mainInfo"))).click();

		TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='stockTabs:org_label']" ) ).click();
		TestFunctions.waitUntilSiteLoaded();

		TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='stockTabs:org_panel']/div/ul/li[" + 2 + "]" ) ).click();

		TestFunctions.findAndWaitOnElement(By.linkText(lang.getProperty("admin.stock.batchAssignRemove"))).click();
		
		TestFunctions.findAndWaitOnElement(
				By.xpath(".//*[@id='stockTabs:selectionRadioBtnTab']/tbody[1]/tr[1]/td[1]/div[1]")).click();

		// test whether data stocks list is not empty and whether
		// desired data set is occurs in assignable data set list
		TestFunctions.findAndWaitOnElement(
				By.xpath(".//*[@id='stockTabs:sourceStockTable_data']/tr[1]/td[1]")).click();
		TestFunctions.findAndWaitOnElement(
				By.xpath(".//*[@id='stockTabs:sourceStockTable_data']/tr[2]/td[1]")).click();
		TestFunctions.findAndWaitOnElement(
				By.xpath(".//*[@id='stockTabs:sourceStockTable_data']/tr[3]/td[1]")).click();
		TestFunctions.findAndWaitOnElement(
				By.xpath(".//*[@id='stockTabs:sourceStockTable_data']/tr[4]/td[1]")).click();
		
		// Checking if all datasets types exist as a checkboxes
		for (DataSetsTypes dataSetsType : DataSetsTypes.values()) {
			String label =TestContext.lang.getProperty(dataSetsType.getDisplayName()  ) ;
			TestFunctions.findAndWaitOnElement( By.xpath( "//td[text()='" + label + "']" ) );
		}

		// click on assign button to assign a data set
		TestFunctions.findAndWaitOnElement(By.id("stockTabs:removeAllBtn")).click();
		
		TestFunctions.findAndWaitOnElement(
				By.xpath(".//*[@id='stockTabs:adAssignAlldetachSelectedFromStockConfirmHeader']/legend[1]")).click();

		TestFunctions.findAndWaitOnElement(By.xpath(
				".//*[@id='stockTabs:adAssignAlldetachSelectedFromStockConfirmHeader']/div[1]/table[1]/tbody[1]/tr[2]/td[1]/table[1]/tbody[1]/tr[4]/td[1]/div[1]"))
				.click();
		
		Thread.sleep(2000);
		TestFunctions.findAndWaitOnElement(
				By.xpath(".//*[@id='stockTabs:adAssignAlldetachSelectedFromStockConfirmOkButton']")).click();
		Thread.sleep(2000);
		
		TestFunctions.goToPageByAdminMenu("admin.jobs", "admin.jobs.showList");
		log.debug("Going to 'Show jobs' page.");
		String state = TestFunctions.getFirstTableEntryAtColumn("showJobsTable", new Long(11));
		
		log.debug("Checking new job entry.");
		TestFunctions.waitUntilSiteLoaded();
		if (!TestFunctions.isLastTableEntryAtColumnEqual("showJobsTable", new Long(2), "admin")) {
			org.testng.Assert.fail("Incorrect user name is shown.");
		}

		if (!TestFunctions.getFirstTableEntryAtColumn("showJobsTable", new Long(3)).equals("remove")) {
			org.testng.Assert.fail("Incorrect job type is shown.");
		}

		if (log.isDebugEnabled())
			log.debug("state is: '" + state + "'");
		while (state.equals("RUNNING")) {
			TestFunctions.clickButtonWithLabel("refresh");
			TestFunctions.waitUntilSiteLoaded();
			state = TestFunctions.getFirstTableEntryAtColumn("showJobsTable", new Long(11));
			log.debug("updating job page");
		}
		TestFunctions.waitUntilSiteLoaded();
		log.debug("job is complete.");

		Long errorCount = Long.valueOf( TestFunctions.getFirstTableEntryAtColumn("showJobsTable", new Long(8))).longValue();
		Long infoCount = Long.valueOf(TestFunctions.getFirstTableEntryAtColumn("showJobsTable", new Long(9))).longValue();
		Long successCount = Long.valueOf(TestFunctions.getFirstTableEntryAtColumn("showJobsTable", new Long(10))).longValue();
		
		TestFunctions.findAndWaitOnElement(By.xpath(".//*[@id='showJobsTable_data']/tr[1]/td[1]/div")).click();
		String jobLog = TestFunctions.getSecondTableEntryAtColumn("showJobsTable", new Long(1));

		log.debug("Checking amount of data sets that were removed correctly.");
		if (errorCount != 0 || successCount != 52 || !(state.equals("COMPLETE"))) {
			log.trace("Error is: " + errorCount);
			log.trace("infosCount is: " + infoCount);
			log.trace("state is: " + state);
			org.testng.Assert.fail("Job did not complete correctly!");
		}
		log.debug("Checking job log.");
			
			Long removedDataSets = new Long(StringUtils.countOccurrencesOf(jobLog, "SUCCESS"));
			if (log.isTraceEnabled()) {
				log.trace("jobLog is: " + jobLog);
				log.trace("number of removed data sets is: " + removedDataSets);
			}
			if (removedDataSets != 53) {
				org.testng.Assert.fail("Incorrect ammount of files was correctly removed!");
			}


		TestFunctions.goToPageByAdminMenu("admin.dataset.manageList", "admin.process.manageList");
		TestFunctions.selectDataStock(stockName);
		TestFunctions.targetTableContainsNumberDataSets("processTable", new Long(0));
		
		TestFunctions.goToPageByAdminMenu("admin.dataset.manageList", "admin.lciaMethod.manageList");
		TestFunctions.targetTableContainsNumberDataSets("lciamethodTable", new Long(0));
		
		TestFunctions.goToPageByAdminMenu("admin.dataset.manageList", "admin.elementaryFlow.manageList");
		TestFunctions.targetTableContainsNumberDataSets("flowTable", new Long(0));
		
		TestFunctions.goToPageByAdminMenu("admin.dataset.manageList", "admin.productFlow.manageList");
		TestFunctions.targetTableContainsNumberDataSets("flowTable", new Long(0));
		
		TestFunctions.goToPageByAdminMenu("admin.dataset.manageList", "admin.flowProperty.manageList");
		TestFunctions.targetTableContainsNumberDataSets("flowpropertyTable", new Long(0));

		TestFunctions.goToPageByAdminMenu("admin.dataset.manageList", "admin.unitGroup.manageList");
		TestFunctions.targetTableContainsNumberDataSets("unitgroupTable", new Long(0));

		TestFunctions.goToPageByAdminMenu("admin.dataset.manageList", "admin.source.manageList");
		TestFunctions.targetTableContainsNumberDataSets("sourceTable", new Long(0));

		TestFunctions.goToPageByAdminMenu("admin.dataset.manageList", "admin.contact.manageList");
		TestFunctions.targetTableContainsNumberDataSets("contactTable", new Long(0));

		login();
	}

}