package edu.kit.soda4lca.test.ui.basic;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import edu.kit.soda4lca.test.ui.AbstractUITest;
import edu.kit.soda4lca.test.ui.main.TestContext;
import edu.kit.soda4lca.test.ui.main.TestFunctions;

/**
 * Try to get redirected to requested page in admin area directly after login
 * 
 * @author sarai.eilebrecht
 * 
 */
public class T003RedirectPageTest extends AbstractUITest {

	// initializing the log
	protected final static Logger log = org.apache.log4j.Logger.getLogger( T001Absolut_BasicTest.class );
	

	/**
	 * try to get to requested page directly after login with different user/password combinations
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public static void redirectpage() throws InterruptedException {
		// Redirect Page test
		log.info( "redirect test started" );
		TestContext.getInstance().getDriver().manage().deleteAllCookies();
		WebDriver driver = TestContext.getInstance().getDriver();
		Properties lang = TestContext.lang;
		driver.get("http://localhost:8080/Node/admin/datasets/manageProcessList.xhtml?stock=default");
		TestFunctions.loginOnLoginSite("admin", "default", false);
		log.debug( "Redirect Page test started - Try to get to Manage Processes page with different credentials" );
		TestFunctions.findAndWaitOnElement( By.id("processTableForm" ) );
		TestFunctions.logout();
		driver.get("http://localhost:8080/Node/admin/datasets/manageProcessList.xhtml?stock=default");
		TestFunctions.loginOnLoginSite("admin", "", false);
		(new WebDriverWait( driver, TestContext.timeout )).until( ExpectedConditions.visibilityOfElementLocated( By.xpath( ".//*[@id='messages']/div" ) ) );
		TestFunctions.findAndWaitOnElement( By.xpath( ".//*[@id='messages']/div" ) );
		if ( TestFunctions.isElementNotPresent( By.linkText( lang.getProperty( "admin.adminArea" ))  )  )
			org.testng.Assert.fail( "Admin area button is presented but password is incorrect." );
		if (TestFunctions.isElementNotPresent(By.id("processTableForm" )))
			org.testng.Assert.fail( "Manage Processes is presented but password is incorrect." );
		log.info( "Redirect Page test finished" );
	}

}

